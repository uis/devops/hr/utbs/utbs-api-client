#!/bin/sh
# Build the deliverables.

cleanup() {
  exit_code=$?
  UID_GID="$(id -u):$(id -g)" docker-compose --project-name utbs-client down
  exit $exit_codea
}
trap cleanup 0 1 2 3 6

# Create build dir, if it doesn't already exist
[ ! -d build ] && mkdir -pv build

UID_GID="$(id -u):$(id -g)" docker-compose --project-name utbs-client up --build --abort-on-container-exit build ${@}
