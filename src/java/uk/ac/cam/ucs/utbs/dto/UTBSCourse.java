/*
Copyright (c) 2013, University of Cambridge Computing Service.

This file is part of the University Training Booking System client library.

This library is free software: you can redistribute it and/or modify
it under the terms of the GNU Lesser General Public License as published
by the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This library is distributed in the hope that it will be useful, but
WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Lesser General Public
License for more details.

You should have received a copy of the GNU Lesser General Public License
along with this library.  If not, see <http://www.gnu.org/licenses/>.
*/
package uk.ac.cam.ucs.utbs.dto;

import java.util.List;

import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElementWrapper;

import uk.ac.cam.ucs.utbs.dto.UTBSResult.EntityMap;

/**
 * Class representing a course returned by the web service API.
 * <p>
 * Note that all the course details are actually on the individual events for
 * the course, and may vary each time the course is run.
 * <p>
 * In rare cases, a course may switch provider, and so there is no provider
 * field on this class, and there is no guarantee that all the course's
 * events have the same provider.
 *
 * @author Dean Rasheed (dev-group@ucs.cam.ac.uk)
 */
public class UTBSCourse
{
    /** The unique identifier of the course. */
    @XmlAttribute public String courseId;

    /**
     * A list of the course's events. This will only be populated if the
     * <code>fetch</code> parameter included the "events" option.
     */
    @XmlElementWrapper
    @XmlElement(name = "event")
    public List<UTBSEvent> events;

    /**
     * A list of the course's themes. This will only be populated if the
     * <code>fetch</code> parameter included the "themes" option.
     */
    @XmlElementWrapper
    @XmlElement(name = "theme")
    public List<UTBSTheme> themes;

    /* == Attributes/methods for the flattened XML/JSON representation == */

    /**
     * An ID that can uniquely identify this course within the returned
     * XML/JSON document. This is only used in the flattened XML/JSON
     * representation (if the "flatten" parameter is specified).
     */
    @XmlAttribute public String id;

    /**
     * A reference (by id) to a course element in the XML/JSON document.
     * This is only used in the flattened XML/JSON representation (if the
     * "flatten" parameter is specified).
     */
    @XmlAttribute public String ref;

    /** Compute an ID that can uniquely identify this entity in XML/JSON. */
    public String id() { return "course/"+courseId; }

    /** Return a new UTBSCourse that refers to this course. */
    public UTBSCourse ref() { UTBSCourse c = new UTBSCourse(); c.ref = id; return c; }

    /* == Code to help clients unflatten a UTBSCourse object == */

    /* Flag to prevent infinite recursion due to circular references. */
    private boolean unflattened;

    /** Unflatten a single UTBSCourse. */
    protected UTBSCourse unflatten(EntityMap    em)
    {
        if (ref != null)
        {
            UTBSCourse course = em.getCourse(ref);
            if (!course.unflattened)
            {
                course.unflattened = true;
                UTBSEvent.unflatten(em, course.events);
                UTBSTheme.unflatten(em, course.themes);
            }
            return course;
        }
        return this;
    }

    /** Unflatten a list of UTBSCourse objects (done in place). */
    protected static void unflatten(EntityMap           em,
                                    List<UTBSCourse>    courses)
    {
        if (courses != null)
            for (int i=0; i<courses.size(); i++)
                courses.set(i, courses.get(i).unflatten(em));
    }
}
