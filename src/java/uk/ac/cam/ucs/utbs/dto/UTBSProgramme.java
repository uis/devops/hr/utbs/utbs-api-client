/*
Copyright (c) 2013, University of Cambridge Computing Service.

This file is part of the University Training Booking System client library.

This library is free software: you can redistribute it and/or modify
it under the terms of the GNU Lesser General Public License as published
by the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This library is distributed in the hope that it will be useful, but
WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Lesser General Public
License for more details.

You should have received a copy of the GNU Lesser General Public License
along with this library.  If not, see <http://www.gnu.org/licenses/>.
*/
package uk.ac.cam.ucs.utbs.dto;

import java.util.Date;
import java.util.List;

import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElementWrapper;

import uk.ac.cam.ucs.utbs.dto.UTBSResult.EntityMap;

/**
 * Class representing a programme of events returned by the web service API.
 *
 * @author Dean Rasheed (dev-group@ucs.cam.ac.uk)
 */
public class UTBSProgramme
{
    /** The unique identifier of the programme. */
    @XmlAttribute public Long programmeId;

    /**
     * The programme's provider. This will only be populated if the
     * <code>fetch</code> parameter included the "provider" option.
     */
    @XmlElement public UTBSProvider provider;

    /** The programme's short name (as it appears in some reports). */
    @XmlElement public String shortName;

    /** The programme's title (as it appears in most of the UTBS). */
    @XmlElement public String title;

    /** The programme's status ("published" or "completed"). */
    @XmlElement public String status;

    /** The programme's start date. */
    @XmlElement public Date startDate;

    /** The programme's end date. */
    @XmlElement public Date endDate;

    /**
     * A list of the programme's events. This will only be populated if the
     * <code>fetch</code> parameter included the "events" option.
     */
    @XmlElementWrapper
    @XmlElement(name = "event")
    public List<UTBSEvent> events;

    /* == Attributes/methods for the flattened XML/JSON representation == */

    /**
     * An ID that can uniquely identify this programme within the returned
     * XML/JSON document. This is only used in the flattened XML/JSON
     * representation (if the "flatten" parameter is specified).
     */
    @XmlAttribute public String id;

    /**
     * A reference (by id) to a programme element in the XML/JSON document.
     * This is only used in the flattened XML/JSON representation (if the
     * "flatten" parameter is specified).
     */
    @XmlAttribute public String ref;

    /** Compute an ID that can uniquely identify this entity in XML/JSON. */
    public String id() { return "prog/"+programmeId; }

    /** Return a new UTBSProgramme that refers to this programme. */
    public UTBSProgramme ref() { UTBSProgramme p = new UTBSProgramme(); p.ref = id; return p; }

    /* == Code to help clients unflatten a UTBSProgramme object == */

    /* Flag to prevent infinite recursion due to circular references. */
    private boolean unflattened;

    /** Unflatten a single UTBSProgramme. */
    protected UTBSProgramme unflatten(EntityMap em)
    {
        if (ref != null)
        {
            UTBSProgramme programme = em.getProgramme(ref);
            if (!programme.unflattened)
            {
                programme.unflattened = true;
                if (programme.provider != null)
                    programme.provider = programme.provider.unflatten(em);
                UTBSEvent.unflatten(em, programme.events);
            }
            return programme;
        }
        return this;
    }

    /** Unflatten a list of UTBSProgramme objects (done in place). */
    protected static void unflatten(EntityMap           em,
                                    List<UTBSProgramme> programmes)
    {
        if (programmes != null)
            for (int i=0; i<programmes.size(); i++)
                programmes.set(i, programmes.get(i).unflatten(em));
    }
}
