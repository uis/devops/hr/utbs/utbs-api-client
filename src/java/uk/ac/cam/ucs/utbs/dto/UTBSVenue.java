/*
Copyright (c) 2013, University of Cambridge Computing Service.

This file is part of the University Training Booking System client library.

This library is free software: you can redistribute it and/or modify
it under the terms of the GNU Lesser General Public License as published
by the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This library is distributed in the hope that it will be useful, but
WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Lesser General Public
License for more details.

You should have received a copy of the GNU Lesser General Public License
along with this library.  If not, see <http://www.gnu.org/licenses/>.
*/
package uk.ac.cam.ucs.utbs.dto;

import java.util.List;

import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;

import uk.ac.cam.ucs.utbs.dto.UTBSResult.EntityMap;

/**
 * Class representing a venue returned by the web service API.
 *
 * @author Dean Rasheed (dev-group@ucs.cam.ac.uk)
 */
public class UTBSVenue
{
    /** The unique identifier of the venue. */
    @XmlAttribute public Long venueId;

    /** The venue's unique short name (as it appears on the timetable). */
    @XmlElement public String shortName;

    /**
     * The venue's provider. This will only be populated if the
     * <code>fetch</code> parameter included the "provider" option.
     */
    @XmlElement public UTBSProvider provider;

    /** The venue's full name. */
    @XmlElement public String name;

    /** The venue's description. */
    @XmlElement public String description;

    /** The number of seats or workstations in the venue. */
    @XmlElement public Integer capacity;

    /** The venue's location (address). */
    @XmlElement public String location;

    /** Directions to get to the venue. */
    @XmlElement public String directions;

    /**
     * Information about access and emergency escape for those with
     * disabilities.
     */
    @XmlElement public String disabledAccess;

    /** Availablity of parking near the venue. */
    @XmlElement public String parkingAdvice;

    /** The URL of a map showing the venue's location. */
    @XmlElement public String mapUrl;

    /* == Attributes/methods for the flattened XML/JSON representation == */

    /**
     * An ID that can uniquely identify this venue within the returned
     * XML/JSON document. This is only used in the flattened XML/JSON
     * representation (if the "flatten" parameter is specified).
     */
    @XmlAttribute public String id;

    /**
     * A reference (by id) to a venue element in the XML/JSON document.
     * This is only used in the flattened XML/JSON representation (if the
     * "flatten" parameter is specified).
     */
    @XmlAttribute public String ref;

    /** Compute an ID that can uniquely identify this entity in XML/JSON. */
    public String id() { return "venue/"+shortName; }

    /** Return a new UTBSVenue that refers to this venue. */
    public UTBSVenue ref() { UTBSVenue v = new UTBSVenue(); v.ref = id; return v; }

    /* == Code to help clients unflatten a UTBSVenue object == */

    /* Flag to prevent infinite recursion due to circular references. */
    private boolean unflattened;

    /** Unflatten a single UTBSVenue. */
    protected UTBSVenue unflatten(EntityMap em)
    {
        if (ref != null)
        {
            UTBSVenue venue = em.getVenue(ref);
            if (!venue.unflattened)
            {
                venue.unflattened = true;
                if (venue.provider != null)
                    venue.provider = venue.provider.unflatten(em);
            }
            return venue;
        }
        return this;
    }

    /** Unflatten a list of UTBSVenue objects (done in place). */
    protected static void unflatten(EntityMap       em,
                                    List<UTBSVenue> venues)
    {
        if (venues != null)
            for (int i=0; i<venues.size(); i++)
                venues.set(i, venues.get(i).unflatten(em));
    }
}
