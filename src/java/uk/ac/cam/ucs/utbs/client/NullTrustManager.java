package uk.ac.cam.ucs.utbs.client;

import java.security.cert.CertificateException;
import java.security.cert.X509Certificate;

import javax.net.ssl.X509TrustManager;

/**
 * Null trust manager for checking X509 certificates. This disables all
 * checking, and just trusts the server.
 * <p>
 * This should only be used for testing, when connecting to a server that is
 * using self-signed certificates.
 */
/* package */ class NullTrustManager implements X509TrustManager
{
    @Override
    public void checkClientTrusted(X509Certificate[]    chain,
                                   String               authType)
        throws CertificateException
    {
        // Do nothing (trust the client unconditionally)
    }

    @Override
    public void checkServerTrusted(X509Certificate[]    chain,
                                   String               authType)
        throws CertificateException
    {
        // Do nothing (trust the server unconditionally)
    }

    @Override
    public X509Certificate[] getAcceptedIssuers()
    {
        return null; // No CA certificates
    }
}
