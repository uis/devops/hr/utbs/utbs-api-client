/*
Copyright (c) 2013, University of Cambridge Computing Service.

This file is part of the University Training Booking System client library.

This library is free software: you can redistribute it and/or modify
it under the terms of the GNU Lesser General Public License as published
by the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This library is distributed in the hope that it will be useful, but
WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Lesser General Public
License for more details.

You should have received a copy of the GNU Lesser General Public License
along with this library.  If not, see <http://www.gnu.org/licenses/>.
*/
package uk.ac.cam.ucs.utbs.client;

import uk.ac.cam.ucs.utbs.dto.UTBSError;

/**
 * Exception thrown when a web service API method fails. This is wrapper
 * around the {@link UTBSError} object returned by the server, which contains
 * the full details of what went wrong.
 *
 * @author Dean Rasheed (dev-group@ucs.cam.ac.uk)
 */
public class UTBSException extends Exception
{
    private static final long serialVersionUID = -3445188800863507675L;
    private UTBSError error;

    /**
     * Construct a new UTBSException wrapping the specified UTBSError.
     *
     * @param error The error from the server.
     */
    public UTBSException(UTBSError  error)
    {
        super(error.message);
        this.error = error;
    }

    /**
     * Returns the underlying error from the server.
     */
    public UTBSError getError()
    {
        return error;
    }
}
