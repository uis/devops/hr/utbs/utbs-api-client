/* === AUTO-GENERATED - DO NOT EDIT === */

/*
Copyright (c) 2013, University of Cambridge Computing Service.

This file is part of the University Training Booking System client library.

This library is free software: you can redistribute it and/or modify
it under the terms of the GNU Lesser General Public License as published
by the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This library is distributed in the hope that it will be useful, but
WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Lesser General Public
License for more details.

You should have received a copy of the GNU Lesser General Public License
along with this library.  If not, see <http://www.gnu.org/licenses/>.
*/
package uk.ac.cam.ucs.utbs.methods;

import java.io.IOException;

import javax.xml.bind.JAXBException;

import uk.ac.cam.ucs.utbs.client.ClientConnection;
import uk.ac.cam.ucs.utbs.client.ClientConnection.Method;
import uk.ac.cam.ucs.utbs.client.UTBSException;
import uk.ac.cam.ucs.utbs.dto.*;

/**
 * Methods for querying and manipulating themes.
 *
 * <h4>The fetch parameter for themes</h4>
 * <p>
 * All methods that return themes also accept an optional <code>fetch</code>
 * parameter that may be used to request additional information about the
 * themes returned. For more details about the general rules that apply to
 * the <code>fetch</code> parameter, refer to the {@link EventMethods}
 * documentation.
 * <p>
 * For themes the <code>fetch</code> parameter may be used to fetch
 * referenced providers, courses and events. The following references are
 * supported:
 * <ul>
 * <li>{@code "provider"} - fetches the theme's provider.</li>
 * <li>{@code "courses"} - fetches the theme's courses.</li>
 * <li>{@code "events"} - fetches all the theme's events, in (date, ID)
 * order.</li>
 * </ul>
 * <p>
 * As with the event <code>fetch</code> parameter, the references may be used
 * in a chain by using the "dot" notation to fetch additional information
 * about referenced providers, courses and events. For example
 * "events.sessions" will fetch all the theme's events and all their
 * sessions. For more information about what can be fetched from referenced
 * providers, courses and events, refer to the documentation for
 * {@link ProviderMethods}, {@link CourseMethods} and {@link EventMethods}.
 *
 * @author Dean Rasheed (dev-group@ucs.cam.ac.uk)
 */
public class ThemeMethods
{
    // The connection to the server
    private ClientConnection conn;

    /**
     * Create a new ThemeMethods object.
     *
     * @param conn The ClientConnection object to use to invoke methods
     * on the server.
     */
    public ThemeMethods(ClientConnection conn)
    {
        this.conn = conn;
    }

    /**
     * Return a list of all themes.
     * <p>
     * By default, only a few basic details about each theme are returned,
     * but the optional <code>fetch</code> parameter may be used to fetch
     * additional attributes or references.
     * <p>
     * <code style="background-color: #eec;">[ HTTP: GET /api/v1/theme/all-themes ]</code>
     *
     * @param fetch [optional] A comma-separated list of any additional
     * details to fetch for each theme.
     *
     * @return The requested themes, in title order.
     */
    public java.util.List<UTBSTheme> getAllThemes(String    fetch)
        throws UTBSException, IOException, JAXBException
    {
        String[] pathParams = {  };
        Object[] queryParams = { "fetch", fetch };
        Object[] formParams = {  };
        UTBSResult result = conn.invokeMethod(Method.GET,
                                              "api/v1/theme/all-themes",
                                              pathParams,
                                              queryParams,
                                              formParams);
        if (result.error != null)
            throw new UTBSException(result.error);
        return result.themes;
    }

    /**
     * Get the themes with the specified name.
     * <p>
     * There may be multiple themes with the same name belonging to different
     * training providers. If a provider name is also supplied, then at most
     * one theme will be returned.
     * <p>
     * <code style="background-color: #eec;">[ HTTP: GET /api/v1/theme/get-by-name/{name} ]</code>
     *
     * @param name [required] The short name of the themes to fetch.
     * @param provName [optional] The short name of a training provider.
     * @param fetch [optional] A comma-separated list of any additional
     * details to fetch for each theme.
     *
     * @return The matching themes, in title order.
     */
    public java.util.List<UTBSTheme> getThemesByName(String name,
                                                     String provName,
                                                     String fetch)
        throws UTBSException, IOException, JAXBException
    {
        String[] pathParams = { name };
        Object[] queryParams = { "provName", provName,
                                 "fetch", fetch };
        Object[] formParams = {  };
        UTBSResult result = conn.invokeMethod(Method.GET,
                                              "api/v1/theme/get-by-name/%1$s",
                                              pathParams,
                                              queryParams,
                                              formParams);
        if (result.error != null)
            throw new UTBSException(result.error);
        return result.themes;
    }

    /**
     * Get the theme with the specified ID.
     * <p>
     * By default, only a few basic details about the theme are returned, but
     * the optional <code>fetch</code> parameter may be used to fetch
     * additional details, such as the events in the theme.
     * <p>
     * <code style="background-color: #eec;">[ HTTP: GET /api/v1/theme/{id} ]</code>
     *
     * @param id [required] The ID of the theme to fetch.
     * @param fetch [optional] A comma-separated list of any additional
     * details to fetch.
     *
     * @return The requested theme or null if it was not found.
     */
    public UTBSTheme getTheme(Long      id,
                              String    fetch)
        throws UTBSException, IOException, JAXBException
    {
        String[] pathParams = { ""+id };
        Object[] queryParams = { "fetch", fetch };
        Object[] formParams = {  };
        UTBSResult result = conn.invokeMethod(Method.GET,
                                              "api/v1/theme/%1$s",
                                              pathParams,
                                              queryParams,
                                              formParams);
        if (result.error != null)
            throw new UTBSException(result.error);
        return result.theme;
    }

    /**
     * Get the events in the specified theme in the specified time period.
     * <p>
     * This will return any events that overlap the specified time period.
     * More specifically, it will return events whose start is less than or
     * equal to the end of the time period, and whose end is greater than or
     * equal to the start of the time period (i.e., all the start and end
     * timestamps are treated inclusively).
     * <p>
     * Optionally, this will also check the event's sessions and exclude any
     * events that have no sessions overlapping the specified time period.
     * This can happen for events with multiple sessions. For example,
     * suppose an event has 2 sessions, one on Monday and the other on
     * Friday, and that the specified time period to search was on Wednesday.
     * Then by default, the event would be returned, because it starts on
     * Monday and ends on Friday, which overlaps the time period being
     * searched, but if session checking is enabled, the event would be
     * excluded.
     * <p>
     * By default, only a few basic details about each event are returned,
     * but the optional <code>fetch</code> parameter may be used to fetch
     * additional attributes or references.
     * <p>
     * NOTE: When using this API directly via the URL endpoints, date-time
     * parameters should be supplied as either milliseconds since epoch, or
     * as ISO 8601 formatted date or date-time strings.
     * <p>
     * <code style="background-color: #eec;">[ HTTP: GET /api/v1/theme/{id}/events-in-time-period ]</code>
     *
     * @param id [required] The ID of the theme.
     * @param start [optional] The start of the time period to search. If
     * omitted, this will default to 0:00am today.
     * @param end [optional] The end of the time period to search. If
     * omitted, this will default to the first midnight after the start date.
     * @param checkSessions [optional] If {@code true}, check the event
     * sessions, and exclude any events that have no sessions overlapping the
     * the time period being searched.
     * @param fetch [optional] A comma-separated list of any additional
     * details to fetch for each event.
     *
     * @return A list of events found, in (start date-time, ID) order.
     */
    public java.util.List<UTBSEvent> getEventsInTimePeriod(Long             id,
                                                           java.util.Date   start,
                                                           java.util.Date   end,
                                                           boolean          checkSessions,
                                                           String           fetch)
        throws UTBSException, IOException, JAXBException
    {
        String[] pathParams = { ""+id };
        Object[] queryParams = { "start", start,
                                 "end", end,
                                 "checkSessions", checkSessions,
                                 "fetch", fetch };
        Object[] formParams = {  };
        UTBSResult result = conn.invokeMethod(Method.GET,
                                              "api/v1/theme/%1$s/events-in-time-period",
                                              pathParams,
                                              queryParams,
                                              formParams);
        if (result.error != null)
            throw new UTBSException(result.error);
        return result.events;
    }

    /**
     * Get a theme's self-taught events.
     * <p>
     * By default, only a few basic details about each event are returned,
     * but the optional <code>fetch</code> parameter may be used to fetch
     * additional attributes or references.
     * <p>
     * <code style="background-color: #eec;">[ HTTP: GET /api/v1/theme/{id}/self-taught-events ]</code>
     *
     * @param id [required] The ID of the theme.
     * @param fetch [optional] A comma-separated list of any additional
     * details to fetch for each event.
     *
     * @return A list of self-taught events, in (title, ID) order.
     */
    public java.util.List<UTBSEvent> getSelfTaughtEvents(Long   id,
                                                         String fetch)
        throws UTBSException, IOException, JAXBException
    {
        String[] pathParams = { ""+id };
        Object[] queryParams = { "fetch", fetch };
        Object[] formParams = {  };
        UTBSResult result = conn.invokeMethod(Method.GET,
                                              "api/v1/theme/%1$s/self-taught-events",
                                              pathParams,
                                              queryParams,
                                              formParams);
        if (result.error != null)
            throw new UTBSException(result.error);
        return result.events;
    }
}
