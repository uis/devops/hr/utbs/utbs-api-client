/* === AUTO-GENERATED - DO NOT EDIT === */

/*
Copyright (c) 2013, University of Cambridge Computing Service.

This file is part of the University Training Booking System client library.

This library is free software: you can redistribute it and/or modify
it under the terms of the GNU Lesser General Public License as published
by the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This library is distributed in the hope that it will be useful, but
WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Lesser General Public
License for more details.

You should have received a copy of the GNU Lesser General Public License
along with this library.  If not, see <http://www.gnu.org/licenses/>.
*/
package uk.ac.cam.ucs.utbs.methods;

import java.io.IOException;

import javax.xml.bind.JAXBException;

import uk.ac.cam.ucs.utbs.client.ClientConnection;
import uk.ac.cam.ucs.utbs.client.ClientConnection.Method;
import uk.ac.cam.ucs.utbs.client.UTBSException;
import uk.ac.cam.ucs.utbs.dto.*;

/**
 * Methods for querying and manipulating institutions.
 *
 * <h4>The fetch parameter for institutions</h4>
 * <p>
 * All methods that return institutions also accept an optional
 * <code>fetch</code> parameter that may be used to request additional
 * information about the institutions returned. For more details about the
 * general rules that apply to the  <code>fetch</code> parameter, refer to the
 * {@link EventMethods} documentation.
 * <p>
 * For institutions the <code>fetch</code> parameter may be used to fetch
 * referenced institutions. The following references are supported:
 * <ul>
 * <li>{@code "parent"} - fetches the institution's parent institution.</li>
 * <li>{@code "school"} - fetches the institution's school institution.</li>
 * <li>{@code "children"} - fetches all the institution's child institutions.
 * This will exclude cancelled children, unless the parent is cancelled.</li>
 * </ul>
 * <p>
 * As with the event <code>fetch</code> parameter, the references may be used
 * in a chain by using the "dot" notation to fetch additional information
 * about referenced institutions. For example "parent.children" will fetch
 * the parent and all sibling institutions.
 *
 * @author Dean Rasheed (dev-group@ucs.cam.ac.uk)
 */
public class InstitutionMethods
{
    // The connection to the server
    private ClientConnection conn;

    /**
     * Create a new InstitutionMethods object.
     *
     * @param conn The ClientConnection object to use to invoke methods
     * on the server.
     */
    public InstitutionMethods(ClientConnection conn)
    {
        this.conn = conn;
    }

    /**
     * Get the institution with the specified ID.
     * <p>
     * By default, only a few basic details about the institution are
     * returned, but the optional <code>fetch</code> parameter may be used to
     * fetch additional details.
     * <p>
     * <code style="background-color: #eec;">[ HTTP: GET /api/v1/institution/{instid} ]</code>
     *
     * @param instid [required] The ID of the institution to fetch.
     * @param fetch [optional] A comma-separated list of any additional
     * details to fetch.
     *
     * @return The requested institution or null if it was not found.
     */
    public UTBSInstitution getInstitution(String    instid,
                                          String    fetch)
        throws UTBSException, IOException, JAXBException
    {
        String[] pathParams = { instid };
        Object[] queryParams = { "fetch", fetch };
        Object[] formParams = {  };
        UTBSResult result = conn.invokeMethod(Method.GET,
                                              "api/v1/institution/%1$s",
                                              pathParams,
                                              queryParams,
                                              formParams);
        if (result.error != null)
            throw new UTBSException(result.error);
        return result.institution;
    }
}
