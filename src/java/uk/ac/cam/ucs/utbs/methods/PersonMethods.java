/* === AUTO-GENERATED - DO NOT EDIT === */

/*
Copyright (c) 2013, University of Cambridge Computing Service.

This file is part of the University Training Booking System client library.

This library is free software: you can redistribute it and/or modify
it under the terms of the GNU Lesser General Public License as published
by the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This library is distributed in the hope that it will be useful, but
WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Lesser General Public
License for more details.

You should have received a copy of the GNU Lesser General Public License
along with this library.  If not, see <http://www.gnu.org/licenses/>.
*/
package uk.ac.cam.ucs.utbs.methods;

import java.io.IOException;

import javax.xml.bind.JAXBException;

import uk.ac.cam.ucs.utbs.client.ClientConnection;
import uk.ac.cam.ucs.utbs.client.ClientConnection.Method;
import uk.ac.cam.ucs.utbs.client.UTBSException;
import uk.ac.cam.ucs.utbs.dto.*;

/**
 * Methods for querying and manipulating people.
 *
 * @author Dean Rasheed (dev-group@ucs.cam.ac.uk)
 */
public class PersonMethods
{
    // The connection to the server
    private ClientConnection conn;

    /**
     * Create a new PersonMethods object.
     *
     * @param conn The ClientConnection object to use to invoke methods
     * on the server.
     */
    public PersonMethods(ClientConnection conn)
    {
        this.conn = conn;
    }

    /**
     * Get the person with the specified CRSid.
     * <p>
     * Note that viewing people requires authentication, and a person's
     * details are only visible to the following:
     * <ul>
     * <li>The person themselves.</li>
     * <li>People with the "view-person-details" privilege.</li>
     * </ul>
     * <p>
     * <code style="background-color: #eec;">[ HTTP: GET /api/v1/person/{crsid} ]</code>
     *
     * @param crsid [required] The CRSid of the person to fetch.
     * @param fetch [optional] A comma-separated list of any additional
     * details to fetch. Currently there are no additional details about
     * people that can be retrieved.
     *
     * @return The requested person or null if they were not found.
     */
    public UTBSPerson getPerson(String  crsid,
                                String  fetch)
        throws UTBSException, IOException, JAXBException
    {
        String[] pathParams = { crsid };
        Object[] queryParams = { "fetch", fetch };
        Object[] formParams = {  };
        UTBSResult result = conn.invokeMethod(Method.GET,
                                              "api/v1/person/%1$s",
                                              pathParams,
                                              queryParams,
                                              formParams);
        if (result.error != null)
            throw new UTBSException(result.error);
        return result.person;
    }

    /**
     * Get the specified person's current bookings.
     * <p>
     * This returns all bookings whose status is not "interested",
     * "cancelled" or "completed" on events that are not finished, and for
     * which the participant's attendance has not yet been recorded.
     * <p>
     * By default, only a few basic details about each booking are returned,
     * but the optional <code>fetch</code> parameter may be used to fetch
     * additional details about each booking.
     * <p>
     * Note that viewing a person's bookings requires authentication as a
     * user with permission to view the person's details. Additionally, the
     * bookings returned are filtered according to whether the user has
     * permission to view them, based on the user's privileges with respect
     * to each booking's event and training provider.
     * <p>
     * <code style="background-color: #eec;">[ HTTP: GET /api/v1/person/{crsid}/current-bookings ]</code>
     *
     * @param crsid [required] The CRSid of the person.
     * @param fetch [optional] A comma-separated list of any additional
     * details to fetch for each booking.
     *
     * @return The person's current bookings, in the order they were created.
     */
    public java.util.List<UTBSEventBooking> getCurrentBookings(String   crsid,
                                                               String   fetch)
        throws UTBSException, IOException, JAXBException
    {
        String[] pathParams = { crsid };
        Object[] queryParams = { "fetch", fetch };
        Object[] formParams = {  };
        UTBSResult result = conn.invokeMethod(Method.GET,
                                              "api/v1/person/%1$s/current-bookings",
                                              pathParams,
                                              queryParams,
                                              formParams);
        if (result.error != null)
            throw new UTBSException(result.error);
        return result.bookings;
    }

    /**
     * Get the specified person's interested bookings.
     * <p>
     * This returns all bookings whose status is "interested". These are the
     * bookings created to register the person's interest in events.
     * <p>
     * By default, only a few basic details about each booking are returned,
     * but the optional <code>fetch</code> parameter may be used to fetch
     * additional details about each booking.
     * <p>
     * Note that viewing a person's bookings requires authentication as a
     * user with permission to view the person's details. Additionally, the
     * bookings returned are filtered according to whether the user has
     * permission to view them, based on the user's privileges with respect
     * to each booking's event and training provider.
     * <p>
     * <code style="background-color: #eec;">[ HTTP: GET /api/v1/person/{crsid}/interested-bookings ]</code>
     *
     * @param crsid [required] The CRSid of the person.
     * @param fetch [optional] A comma-separated list of any additional
     * details to fetch for each booking.
     *
     * @return The person's interested bookings, in the order they were
     * created.
     */
    public java.util.List<UTBSEventBooking> getInterestedBookings(String    crsid,
                                                                  String    fetch)
        throws UTBSException, IOException, JAXBException
    {
        String[] pathParams = { crsid };
        Object[] queryParams = { "fetch", fetch };
        Object[] formParams = {  };
        UTBSResult result = conn.invokeMethod(Method.GET,
                                              "api/v1/person/%1$s/interested-bookings",
                                              pathParams,
                                              queryParams,
                                              formParams);
        if (result.error != null)
            throw new UTBSException(result.error);
        return result.bookings;
    }

    /**
     * Get the specified person's training history.
     * <p>
     * This returns all the person's bookings except their current bookings,
     * as returned by
     * {@link #getCurrentBookings(String,String) getCurrentBookings()} and
     * their bookings with a status of "interested", as returned by
     * {@link #getInterestedBookings(String,String) getInterestedBookings()}.
     * Note that this may include cancelled bookings and bookings on events
     * that the participant did not attend.
     * <p>
     * By default, only a few basic details about each booking are returned,
     * but the optional <code>fetch</code> parameter may be used to fetch
     * additional details about each booking.
     * <p>
     * Note that viewing a person's bookings requires authentication as a
     * user with permission to view the person's details. Additionally, the
     * bookings returned are filtered according to whether the user has
     * permission to view them, based on the user's privileges with respect
     * to each booking's event and training provider.
     * <p>
     * <code style="background-color: #eec;">[ HTTP: GET /api/v1/person/{crsid}/training-history ]</code>
     *
     * @param crsid [required] The CRSid of the person.
     * @param fetch [optional] A comma-separated list of any additional
     * details to fetch for each booking.
     *
     * @return The person's training history, in the order in which the
     * bookings were created.
     */
    public java.util.List<UTBSEventBooking> getTrainingHistory(String   crsid,
                                                               String   fetch)
        throws UTBSException, IOException, JAXBException
    {
        String[] pathParams = { crsid };
        Object[] queryParams = { "fetch", fetch };
        Object[] formParams = {  };
        UTBSResult result = conn.invokeMethod(Method.GET,
                                              "api/v1/person/%1$s/training-history",
                                              pathParams,
                                              queryParams,
                                              formParams);
        if (result.error != null)
            throw new UTBSException(result.error);
        return result.bookings;
    }
}
