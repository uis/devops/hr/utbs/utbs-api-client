/* === AUTO-GENERATED - DO NOT EDIT === */

/*
Copyright (c) 2013, University of Cambridge Computing Service.

This file is part of the University Training Booking System client library.

This library is free software: you can redistribute it and/or modify
it under the terms of the GNU Lesser General Public License as published
by the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This library is distributed in the hope that it will be useful, but
WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Lesser General Public
License for more details.

You should have received a copy of the GNU Lesser General Public License
along with this library.  If not, see <http://www.gnu.org/licenses/>.
*/
package uk.ac.cam.ucs.utbs.methods;

import java.io.IOException;

import javax.xml.bind.JAXBException;

import uk.ac.cam.ucs.utbs.client.ClientConnection;
import uk.ac.cam.ucs.utbs.client.ClientConnection.Method;
import uk.ac.cam.ucs.utbs.client.UTBSException;
import uk.ac.cam.ucs.utbs.dto.*;

/**
 * Methods for querying and manipulating training providers.
 *
 * <h4>The fetch parameter for providers</h4>
 * <p>
 * All methods that return providers also accept an optional
 * <code>fetch</code> parameter that may be used to request additional
 * information about the providers returned. For more details about the
 * general rules that apply to the <code>fetch</code> parameter, refer to the
 * {@link EventMethods} documentation.
 * <p>
 * For providers the <code>fetch</code> parameter may be used to fetch
 * referenced programmes, themes and venues. The following references are
 * supported:
 * <ul>
 * <li>{@code "institution"} - fetches full details about the provider's
 * institution.</li>
 * <li>{@code "current_programme"} - fetches the provider's current
 * programme, if there is one.</li>
 * <li>{@code "programmes"} - fetches all the provider's programmes, in date
 * order.</li>
 * <li>{@code "themes"} - fetches all the provider's themes.</li>
 * <li>{@code "venues"} - fetches all the venues belonging to the
 * provider.</li>
 * </ul>
 * <p>
 * As with the event <code>fetch</code> parameter, the references may be used
 * in a chain by using the "dot" notation to fetch additional information
 * about referenced programmes, themes and venues. For example
 * "themes.courses" will fetch all the courses for each of the provider's
 * themes. For more information about what can be fetched from referenced
 * programmes, themes and venues, refer to the documentation for
 * {@link ProgrammeMethods}, {@link ThemeMethods} and {@link VenueMethods}.
 *
 * @author Dean Rasheed (dev-group@ucs.cam.ac.uk)
 */
public class ProviderMethods
{
    // The connection to the server
    private ClientConnection conn;

    /**
     * Create a new ProviderMethods object.
     *
     * @param conn The ClientConnection object to use to invoke methods
     * on the server.
     */
    public ProviderMethods(ClientConnection conn)
    {
        this.conn = conn;
    }

    /**
     * Return a list of all training providers.
     * <p>
     * By default, only a few basic details about each provider are returned,
     * but the optional <code>fetch</code> parameter may be used to fetch
     * additional attributes or references.
     * <p>
     * <code style="background-color: #eec;">[ HTTP: GET /api/v1/provider/all-providers ]</code>
     *
     * @param fetch [optional] A comma-separated list of any additional
     * details to fetch for each provider.
     *
     * @return The requested providers (in name order).
     */
    public java.util.List<UTBSProvider> getAllProviders(String  fetch)
        throws UTBSException, IOException, JAXBException
    {
        String[] pathParams = {  };
        Object[] queryParams = { "fetch", fetch };
        Object[] formParams = {  };
        UTBSResult result = conn.invokeMethod(Method.GET,
                                              "api/v1/provider/all-providers",
                                              pathParams,
                                              queryParams,
                                              formParams);
        if (result.error != null)
            throw new UTBSException(result.error);
        return result.providers;
    }

    /**
     * Get the training provider with the specified short name (e.g., "UCS").
     * <p>
     * By default, only a few basic details about the provider are returned,
     * but the optional <code>fetch</code> parameter may be used to fetch
     * additional attributes or references.
     * <p>
     * <code style="background-color: #eec;">[ HTTP: GET /api/v1/provider/{shortName} ]</code>
     *
     * @param shortName [required] The short name of the provider to fetch.
     * @param fetch [optional] A comma-separated list of any additional
     * details to fetch.
     *
     * @return The requested provider or null if it was not found.
     */
    public UTBSProvider getProvider(String  shortName,
                                    String  fetch)
        throws UTBSException, IOException, JAXBException
    {
        String[] pathParams = { shortName };
        Object[] queryParams = { "fetch", fetch };
        Object[] formParams = {  };
        UTBSResult result = conn.invokeMethod(Method.GET,
                                              "api/v1/provider/%1$s",
                                              pathParams,
                                              queryParams,
                                              formParams);
        if (result.error != null)
            throw new UTBSException(result.error);
        return result.provider;
    }

    /**
     * Get the events run by the specified training provider in the specified
     * time period.
     * <p>
     * This will return any events that overlap the specified time period.
     * More specifically, it will return events whose start is less than or
     * equal to the end of the time period, and whose end is greater than or
     * equal to the start of the time period (i.e., all the start and end
     * timestamps are treated inclusively).
     * <p>
     * Optionally, this will also check the event's sessions and exclude any
     * events that have no sessions overlapping the specified time period.
     * This can happen for events with multiple sessions. For example,
     * suppose an event has 2 sessions, one on Monday and the other on
     * Friday, and that the specified time period to search was on Wednesday.
     * Then by default, the event would be returned, because it starts on
     * Monday and ends on Friday, which overlaps the time period being
     * searched, but if session checking is enabled, the event would be
     * excluded.
     * <p>
     * By default, only a few basic details about each event are returned,
     * but the optional <code>fetch</code> parameter may be used to fetch
     * additional attributes or references.
     * <p>
     * NOTE: When using this API directly via the URL endpoints, date-time
     * parameters should be supplied as either milliseconds since epoch, or
     * as ISO 8601 formatted date or date-time strings.
     * <p>
     * <code style="background-color: #eec;">[ HTTP: GET /api/v1/provider/{shortName}/events-in-time-period ]</code>
     *
     * @param shortName [required] The short name of the provider.
     * @param start [optional] The start of the time period to search. If
     * omitted, this will default to 0:00am today.
     * @param end [optional] The end of the time period to search. If
     * omitted, this will default to the first midnight after the start date.
     * @param checkSessions [optional] If {@code true}, check the event
     * sessions, and exclude any events that have no sessions overlapping the
     * the time period being searched.
     * @param fetch [optional] A comma-separated list of any additional
     * details to fetch for each event.
     *
     * @return A list of events found, in (start date-time, ID) order.
     */
    public java.util.List<UTBSEvent> getEventsInTimePeriod(String           shortName,
                                                           java.util.Date   start,
                                                           java.util.Date   end,
                                                           boolean          checkSessions,
                                                           String           fetch)
        throws UTBSException, IOException, JAXBException
    {
        String[] pathParams = { shortName };
        Object[] queryParams = { "start", start,
                                 "end", end,
                                 "checkSessions", checkSessions,
                                 "fetch", fetch };
        Object[] formParams = {  };
        UTBSResult result = conn.invokeMethod(Method.GET,
                                              "api/v1/provider/%1$s/events-in-time-period",
                                              pathParams,
                                              queryParams,
                                              formParams);
        if (result.error != null)
            throw new UTBSException(result.error);
        return result.events;
    }

    /**
     * Get all the programmes run by the specified training provider.
     * <p>
     * By default, only a few basic details about each programme are
     * returned, but the optional <code>fetch</code> parameter may be used to
     * fetch additional attributes or references of each programme.
     * <p>
     * <code style="background-color: #eec;">[ HTTP: GET /api/v1/provider/{shortName}/programmes ]</code>
     *
     * @param shortName [required] The short name of the provider.
     * @param fetch [optional] A comma-separated list of any additional
     * details to fetch for each programme.
     *
     * @return The provider's programmes, in (date, ID) order.
     */
    public java.util.List<UTBSProgramme> getProgrammes(String   shortName,
                                                       String   fetch)
        throws UTBSException, IOException, JAXBException
    {
        String[] pathParams = { shortName };
        Object[] queryParams = { "fetch", fetch };
        Object[] formParams = {  };
        UTBSResult result = conn.invokeMethod(Method.GET,
                                              "api/v1/provider/%1$s/programmes",
                                              pathParams,
                                              queryParams,
                                              formParams);
        if (result.error != null)
            throw new UTBSException(result.error);
        return result.programmes;
    }

    /**
     * Get the programmes run by the specified training provider in the
     * specified date range.
     * <p>
     * This will return any programmes that overlap the specified date range.
     * More specifically, it will return programmes whose start is less than
     * or equal to the end date specified, and whose end is greater than or
     * equal to the start date specified (i.e., all the start and end dates
     * are treated inclusively).
     * <p>
     * By default, only a few basic details about each programme are
     * returned, but the optional <code>fetch</code> parameter may be used to
     * fetch additional attributes or references.
     * <p>
     * NOTE: When using this API directly via the URL endpoints, date
     * parameters should be supplied as either milliseconds since epoch, or
     * as ISO 8601 formatted date or date-time strings.
     * <p>
     * <code style="background-color: #eec;">[ HTTP: GET /api/v1/provider/{shortName}/programmes-by-date ]</code>
     *
     * @param shortName [required] The short name of the provider.
     * @param startDate [optional] The start date. If omitted, this will
     * default to today.
     * @param endDate [optional] The end date. If omitted, this will default
     * to today.
     * @param fetch [optional] A comma-separated list of any additional
     * details to fetch for each programme.
     *
     * @return A list of programmes found, in (date, ID) order.
     */
    public java.util.List<UTBSProgramme> getProgrammesByDate(String         shortName,
                                                             java.util.Date startDate,
                                                             java.util.Date endDate,
                                                             String         fetch)
        throws UTBSException, IOException, JAXBException
    {
        String[] pathParams = { shortName };
        Object[] queryParams = { "startDate", startDate,
                                 "endDate", endDate,
                                 "fetch", fetch };
        Object[] formParams = {  };
        UTBSResult result = conn.invokeMethod(Method.GET,
                                              "api/v1/provider/%1$s/programmes-by-date",
                                              pathParams,
                                              queryParams,
                                              formParams);
        if (result.error != null)
            throw new UTBSException(result.error);
        return result.programmes;
    }

    /**
     * Get all the themes belonging to the specified training provider.
     * <p>
     * By default, only a few basic details about each theme are returned,
     * but the optional <code>fetch</code> parameter may be used to fetch
     * additional attributes or references of each programme.
     * <p>
     * <code style="background-color: #eec;">[ HTTP: GET /api/v1/provider/{shortName}/themes ]</code>
     *
     * @param shortName [required] The short name of the provider.
     * @param fetch [optional] A comma-separated list of any additional
     * details to fetch for each theme.
     *
     * @return The provider's themes, in title order.
     */
    public java.util.List<UTBSTheme> getThemes(String   shortName,
                                               String   fetch)
        throws UTBSException, IOException, JAXBException
    {
        String[] pathParams = { shortName };
        Object[] queryParams = { "fetch", fetch };
        Object[] formParams = {  };
        UTBSResult result = conn.invokeMethod(Method.GET,
                                              "api/v1/provider/%1$s/themes",
                                              pathParams,
                                              queryParams,
                                              formParams);
        if (result.error != null)
            throw new UTBSException(result.error);
        return result.themes;
    }

    /**
     * Get all the venues belonging to the specified training provider.
     * <p>
     * By default, only a few basic details about each venue are returned,
     * but the optional <code>fetch</code> parameter may be used to fetch
     * additional attributes or references of each programme.
     * <p>
     * <code style="background-color: #eec;">[ HTTP: GET /api/v1/provider/{shortName}/venues ]</code>
     *
     * @param shortName [required] The short name of the provider.
     * @param fetch [optional] A comma-separated list of any additional
     * details to fetch for each venue.
     *
     * @return The provider's venues, in name order.
     */
    public java.util.List<UTBSVenue> getVenues(String   shortName,
                                               String   fetch)
        throws UTBSException, IOException, JAXBException
    {
        String[] pathParams = { shortName };
        Object[] queryParams = { "fetch", fetch };
        Object[] formParams = {  };
        UTBSResult result = conn.invokeMethod(Method.GET,
                                              "api/v1/provider/%1$s/venues",
                                              pathParams,
                                              queryParams,
                                              formParams);
        if (result.error != null)
            throw new UTBSException(result.error);
        return result.venues;
    }
}
