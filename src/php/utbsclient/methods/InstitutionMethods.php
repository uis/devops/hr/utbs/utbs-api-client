<?php
/* === AUTO-GENERATED - DO NOT EDIT === */

/*
Copyright (c) 2013, University of Cambridge Computing Service.

This file is part of the University Training Booking System client library.

This library is free software: you can redistribute it and/or modify
it under the terms of the GNU Lesser General Public License as published
by the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This library is distributed in the hope that it will be useful, but
WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Lesser General Public
License for more details.

You should have received a copy of the GNU Lesser General Public License
along with this library.  If not, see <http://www.gnu.org/licenses/>.
*/

require_once dirname(__FILE__) . "/../client/UTBSException.php";

/**
 * Methods for querying and manipulating institutions.
 *
 * **The fetch parameter for institutions**
 *
 * All methods that return institutions also accept an optional
 * ``fetch`` parameter that may be used to request additional
 * information about the institutions returned. For more details about the
 * general rules that apply to the  ``fetch`` parameter, refer to the
 * {@link EventMethods} documentation.
 *
 * For institutions the ``fetch`` parameter may be used to fetch
 * referenced institutions. The following references are supported:
 *
 * * ``"parent"`` - fetches the institution's parent institution.
 *
 * * ``"school"`` - fetches the institution's school institution.
 *
 * * ``"children"`` - fetches all the institution's child institutions.
 *   This will exclude cancelled children, unless the parent is cancelled.
 *
 * As with the event ``fetch`` parameter, the references may be used
 * in a chain by using the "dot" notation to fetch additional information
 * about referenced institutions. For example "parent.children" will fetch
 * the parent and all sibling institutions.
 *
 * @author Dean Rasheed (dev-group@ucs.cam.ac.uk)
 */
class InstitutionMethods
{
    // The connection to the server
    private $conn;

    /**
     * Create a new InstitutionMethods object.
     *
     * @param ClientConnection $conn The ClientConnection object to use to
     * invoke methods on the server.
     */
    public function __construct($conn)
    {
        $this->conn = $conn;
    }

    /**
     * Get the institution with the specified ID.
     *
     * By default, only a few basic details about the institution are
     * returned, but the optional ``fetch`` parameter may be used to
     * fetch additional details.
     *
     * `` ``
     *
     * ``[ HTTP: GET /api/v1/institution/{instid} ]``
     *
     * @param string $instid [required] The ID of the institution to fetch.
     * @param string $fetch [optional] A comma-separated list of any additional
     * details to fetch.
     *
     * @return UTBSInstitution The requested institution or null if it was not found.
     */
    public function getInstitution($instid,
                                   $fetch=null)
    {
        $pathParams = array("instid" => $instid);
        $queryParams = array("fetch" => $fetch);
        $formParams = array();
        $result = $this->conn->invokeMethod("GET",
                                            'api/v1/institution/%1$s',
                                            $pathParams,
                                            $queryParams,
                                            $formParams);
        if (isset($result->error))
            throw new UTBSException($result->error);
        return $result->institution;
    }
}
