<?php
/* === AUTO-GENERATED - DO NOT EDIT === */

/*
Copyright (c) 2013, University of Cambridge Computing Service.

This file is part of the University Training Booking System client library.

This library is free software: you can redistribute it and/or modify
it under the terms of the GNU Lesser General Public License as published
by the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This library is distributed in the hope that it will be useful, but
WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Lesser General Public
License for more details.

You should have received a copy of the GNU Lesser General Public License
along with this library.  If not, see <http://www.gnu.org/licenses/>.
*/

require_once dirname(__FILE__) . "/../client/UTBSException.php";

/**
 * Common methods for searching for objects in the UTBS database.
 *
 * @author Dean Rasheed (dev-group@ucs.cam.ac.uk)
 */
class UTBSMethods
{
    // The connection to the server
    private $conn;

    /**
     * Create a new UTBSMethods object.
     *
     * @param ClientConnection $conn The ClientConnection object to use to
     * invoke methods on the server.
     */
    public function __construct($conn)
    {
        $this->conn = $conn;
    }

    /**
     * Get the currently authenticated API user.
     *
     * `` ``
     *
     * ``[ HTTP: GET /api/v1/current-user ]``
     *
     * @return String The API user, or null if authentication details have not been
     * supplied.
     */
    public function getCurrentUser()
    {
        $pathParams = array();
        $queryParams = array();
        $formParams = array();
        $result = $this->conn->invokeMethod("GET",
                                            'api/v1/current-user',
                                            $pathParams,
                                            $queryParams,
                                            $formParams);
        if (isset($result->error))
            throw new UTBSException($result->error);
        return $result->value;
    }

    /**
     * Get full list of privileges that the currently authenticated user has,
     * including role privileges.
     *
     * If a provider is not specified, this will only include privileges
     * granted to all providers. Otherwise it will include privileges granted
     * for the specified provider, in addition to all-provider privileges.
     *
     * `` ``
     *
     * ``[ HTTP: GET /api/v1/privileges ]``
     *
     * @param string $provider [optional] The short name of a provider.
     * @return String The user's privileges (separated by newlines).
     */
    public function getPrivileges($provider=null)
    {
        $pathParams = array();
        $queryParams = array("provider" => $provider);
        $formParams = array();
        $result = $this->conn->invokeMethod("GET",
                                            'api/v1/privileges',
                                            $pathParams,
                                            $queryParams,
                                            $formParams);
        if (isset($result->error))
            throw new UTBSException($result->error);
        return $result->value;
    }

    /**
     * Get the current API version number.
     *
     * `` ``
     *
     * ``[ HTTP: GET /api/v1/version ]``
     *
     * @return String The API version number string.
     */
    public function getVersion()
    {
        $pathParams = array();
        $queryParams = array();
        $formParams = array();
        $result = $this->conn->invokeMethod("GET",
                                            'api/v1/version',
                                            $pathParams,
                                            $queryParams,
                                            $formParams);
        if (isset($result->error))
            throw new UTBSException($result->error);
        return $result->value;
    }
}
