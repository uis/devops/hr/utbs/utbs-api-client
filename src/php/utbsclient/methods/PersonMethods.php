<?php
/* === AUTO-GENERATED - DO NOT EDIT === */

/*
Copyright (c) 2013, University of Cambridge Computing Service.

This file is part of the University Training Booking System client library.

This library is free software: you can redistribute it and/or modify
it under the terms of the GNU Lesser General Public License as published
by the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This library is distributed in the hope that it will be useful, but
WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Lesser General Public
License for more details.

You should have received a copy of the GNU Lesser General Public License
along with this library.  If not, see <http://www.gnu.org/licenses/>.
*/

require_once dirname(__FILE__) . "/../client/UTBSException.php";

/**
 * Methods for querying and manipulating people.
 *
 * @author Dean Rasheed (dev-group@ucs.cam.ac.uk)
 */
class PersonMethods
{
    // The connection to the server
    private $conn;

    /**
     * Create a new PersonMethods object.
     *
     * @param ClientConnection $conn The ClientConnection object to use to
     * invoke methods on the server.
     */
    public function __construct($conn)
    {
        $this->conn = $conn;
    }

    /**
     * Get the person with the specified CRSid.
     *
     * Note that viewing people requires authentication, and a person's
     * details are only visible to the following:
     *
     * * The person themselves.
     *
     * * People with the "view-person-details" privilege.
     *
     * `` ``
     *
     * ``[ HTTP: GET /api/v1/person/{crsid} ]``
     *
     * @param string $crsid [required] The CRSid of the person to fetch.
     * @param string $fetch [optional] A comma-separated list of any additional
     * details to fetch. Currently there are no additional details about
     * people that can be retrieved.
     *
     * @return UTBSPerson The requested person or null if they were not found.
     */
    public function getPerson($crsid,
                              $fetch=null)
    {
        $pathParams = array("crsid" => $crsid);
        $queryParams = array("fetch" => $fetch);
        $formParams = array();
        $result = $this->conn->invokeMethod("GET",
                                            'api/v1/person/%1$s',
                                            $pathParams,
                                            $queryParams,
                                            $formParams);
        if (isset($result->error))
            throw new UTBSException($result->error);
        return $result->person;
    }

    /**
     * Get the specified person's current bookings.
     *
     * This returns all bookings whose status is not "interested",
     * "cancelled" or "completed" on events that are not finished, and for
     * which the participant's attendance has not yet been recorded.
     *
     * By default, only a few basic details about each booking are returned,
     * but the optional ``fetch`` parameter may be used to fetch
     * additional details about each booking.
     *
     * Note that viewing a person's bookings requires authentication as a
     * user with permission to view the person's details. Additionally, the
     * bookings returned are filtered according to whether the user has
     * permission to view them, based on the user's privileges with respect
     * to each booking's event and training provider.
     *
     * `` ``
     *
     * ``[ HTTP: GET /api/v1/person/{crsid}/current-bookings ]``
     *
     * @param string $crsid [required] The CRSid of the person.
     * @param string $fetch [optional] A comma-separated list of any additional
     * details to fetch for each booking.
     *
     * @return UTBSEventBooking[] The person's current bookings, in the order they were created.
     */
    public function getCurrentBookings($crsid,
                                       $fetch=null)
    {
        $pathParams = array("crsid" => $crsid);
        $queryParams = array("fetch" => $fetch);
        $formParams = array();
        $result = $this->conn->invokeMethod("GET",
                                            'api/v1/person/%1$s/current-bookings',
                                            $pathParams,
                                            $queryParams,
                                            $formParams);
        if (isset($result->error))
            throw new UTBSException($result->error);
        return $result->bookings;
    }

    /**
     * Get the specified person's interested bookings.
     *
     * This returns all bookings whose status is "interested". These are the
     * bookings created to register the person's interest in events.
     *
     * By default, only a few basic details about each booking are returned,
     * but the optional ``fetch`` parameter may be used to fetch
     * additional details about each booking.
     *
     * Note that viewing a person's bookings requires authentication as a
     * user with permission to view the person's details. Additionally, the
     * bookings returned are filtered according to whether the user has
     * permission to view them, based on the user's privileges with respect
     * to each booking's event and training provider.
     *
     * `` ``
     *
     * ``[ HTTP: GET /api/v1/person/{crsid}/interested-bookings ]``
     *
     * @param string $crsid [required] The CRSid of the person.
     * @param string $fetch [optional] A comma-separated list of any additional
     * details to fetch for each booking.
     *
     * @return UTBSEventBooking[] The person's interested bookings, in the order they were
     * created.
     */
    public function getInterestedBookings($crsid,
                                          $fetch=null)
    {
        $pathParams = array("crsid" => $crsid);
        $queryParams = array("fetch" => $fetch);
        $formParams = array();
        $result = $this->conn->invokeMethod("GET",
                                            'api/v1/person/%1$s/interested-bookings',
                                            $pathParams,
                                            $queryParams,
                                            $formParams);
        if (isset($result->error))
            throw new UTBSException($result->error);
        return $result->bookings;
    }

    /**
     * Get the specified person's training history.
     *
     * This returns all the person's bookings except their current bookings,
     * as returned by
     * {@link getCurrentBookings()} and
     * their bookings with a status of "interested", as returned by
     * {@link getInterestedBookings()}.
     * Note that this may include cancelled bookings and bookings on events
     * that the participant did not attend.
     *
     * By default, only a few basic details about each booking are returned,
     * but the optional ``fetch`` parameter may be used to fetch
     * additional details about each booking.
     *
     * Note that viewing a person's bookings requires authentication as a
     * user with permission to view the person's details. Additionally, the
     * bookings returned are filtered according to whether the user has
     * permission to view them, based on the user's privileges with respect
     * to each booking's event and training provider.
     *
     * `` ``
     *
     * ``[ HTTP: GET /api/v1/person/{crsid}/training-history ]``
     *
     * @param string $crsid [required] The CRSid of the person.
     * @param string $fetch [optional] A comma-separated list of any additional
     * details to fetch for each booking.
     *
     * @return UTBSEventBooking[] The person's training history, in the order in which the
     * bookings were created.
     */
    public function getTrainingHistory($crsid,
                                       $fetch=null)
    {
        $pathParams = array("crsid" => $crsid);
        $queryParams = array("fetch" => $fetch);
        $formParams = array();
        $result = $this->conn->invokeMethod("GET",
                                            'api/v1/person/%1$s/training-history',
                                            $pathParams,
                                            $queryParams,
                                            $formParams);
        if (isset($result->error))
            throw new UTBSException($result->error);
        return $result->bookings;
    }
}
