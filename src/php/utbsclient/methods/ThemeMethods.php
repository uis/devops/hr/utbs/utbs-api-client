<?php
/* === AUTO-GENERATED - DO NOT EDIT === */

/*
Copyright (c) 2013, University of Cambridge Computing Service.

This file is part of the University Training Booking System client library.

This library is free software: you can redistribute it and/or modify
it under the terms of the GNU Lesser General Public License as published
by the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This library is distributed in the hope that it will be useful, but
WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Lesser General Public
License for more details.

You should have received a copy of the GNU Lesser General Public License
along with this library.  If not, see <http://www.gnu.org/licenses/>.
*/

require_once dirname(__FILE__) . "/../client/UTBSException.php";

/**
 * Methods for querying and manipulating themes.
 *
 * **The fetch parameter for themes**
 *
 * All methods that return themes also accept an optional ``fetch``
 * parameter that may be used to request additional information about the
 * themes returned. For more details about the general rules that apply to
 * the ``fetch`` parameter, refer to the {@link EventMethods}
 * documentation.
 *
 * For themes the ``fetch`` parameter may be used to fetch
 * referenced providers, courses and events. The following references are
 * supported:
 *
 * * ``"provider"`` - fetches the theme's provider.
 *
 * * ``"courses"`` - fetches the theme's courses.
 *
 * * ``"events"`` - fetches all the theme's events, in (date, ID)
 *   order.
 *
 * As with the event ``fetch`` parameter, the references may be used
 * in a chain by using the "dot" notation to fetch additional information
 * about referenced providers, courses and events. For example
 * "events.sessions" will fetch all the theme's events and all their
 * sessions. For more information about what can be fetched from referenced
 * providers, courses and events, refer to the documentation for
 * {@link ProviderMethods}, {@link CourseMethods} and {@link EventMethods}.
 *
 * @author Dean Rasheed (dev-group@ucs.cam.ac.uk)
 */
class ThemeMethods
{
    // The connection to the server
    private $conn;

    /**
     * Create a new ThemeMethods object.
     *
     * @param ClientConnection $conn The ClientConnection object to use to
     * invoke methods on the server.
     */
    public function __construct($conn)
    {
        $this->conn = $conn;
    }

    /**
     * Return a list of all themes.
     *
     * By default, only a few basic details about each theme are returned,
     * but the optional ``fetch`` parameter may be used to fetch
     * additional attributes or references.
     *
     * `` ``
     *
     * ``[ HTTP: GET /api/v1/theme/all-themes ]``
     *
     * @param string $fetch [optional] A comma-separated list of any additional
     * details to fetch for each theme.
     *
     * @return UTBSTheme[] The requested themes, in title order.
     */
    public function getAllThemes($fetch=null)
    {
        $pathParams = array();
        $queryParams = array("fetch" => $fetch);
        $formParams = array();
        $result = $this->conn->invokeMethod("GET",
                                            'api/v1/theme/all-themes',
                                            $pathParams,
                                            $queryParams,
                                            $formParams);
        if (isset($result->error))
            throw new UTBSException($result->error);
        return $result->themes;
    }

    /**
     * Get the themes with the specified name.
     *
     * There may be multiple themes with the same name belonging to different
     * training providers. If a provider name is also supplied, then at most
     * one theme will be returned.
     *
     * `` ``
     *
     * ``[ HTTP: GET /api/v1/theme/get-by-name/{name} ]``
     *
     * @param string $name [required] The short name of the themes to fetch.
     * @param string $provName [optional] The short name of a training provider.
     * @param string $fetch [optional] A comma-separated list of any additional
     * details to fetch for each theme.
     *
     * @return UTBSTheme[] The matching themes, in title order.
     */
    public function getThemesByName($name,
                                    $provName=null,
                                    $fetch=null)
    {
        $pathParams = array("name" => $name);
        $queryParams = array("provName" => $provName,
                             "fetch"    => $fetch);
        $formParams = array();
        $result = $this->conn->invokeMethod("GET",
                                            'api/v1/theme/get-by-name/%1$s',
                                            $pathParams,
                                            $queryParams,
                                            $formParams);
        if (isset($result->error))
            throw new UTBSException($result->error);
        return $result->themes;
    }

    /**
     * Get the theme with the specified ID.
     *
     * By default, only a few basic details about the theme are returned, but
     * the optional ``fetch`` parameter may be used to fetch
     * additional details, such as the events in the theme.
     *
     * `` ``
     *
     * ``[ HTTP: GET /api/v1/theme/{id} ]``
     *
     * @param int $id [required] The ID of the theme to fetch.
     * @param string $fetch [optional] A comma-separated list of any additional
     * details to fetch.
     *
     * @return UTBSTheme The requested theme or null if it was not found.
     */
    public function getTheme($id,
                             $fetch=null)
    {
        $pathParams = array("id" => $id);
        $queryParams = array("fetch" => $fetch);
        $formParams = array();
        $result = $this->conn->invokeMethod("GET",
                                            'api/v1/theme/%1$s',
                                            $pathParams,
                                            $queryParams,
                                            $formParams);
        if (isset($result->error))
            throw new UTBSException($result->error);
        return $result->theme;
    }

    /**
     * Get the events in the specified theme in the specified time period.
     *
     * This will return any events that overlap the specified time period.
     * More specifically, it will return events whose start is less than or
     * equal to the end of the time period, and whose end is greater than or
     * equal to the start of the time period (i.e., all the start and end
     * timestamps are treated inclusively).
     *
     * Optionally, this will also check the event's sessions and exclude any
     * events that have no sessions overlapping the specified time period.
     * This can happen for events with multiple sessions. For example,
     * suppose an event has 2 sessions, one on Monday and the other on
     * Friday, and that the specified time period to search was on Wednesday.
     * Then by default, the event would be returned, because it starts on
     * Monday and ends on Friday, which overlaps the time period being
     * searched, but if session checking is enabled, the event would be
     * excluded.
     *
     * By default, only a few basic details about each event are returned,
     * but the optional ``fetch`` parameter may be used to fetch
     * additional attributes or references.
     *
     * NOTE: When using this API directly via the URL endpoints, date-time
     * parameters should be supplied as either milliseconds since epoch, or
     * as ISO 8601 formatted date or date-time strings.
     *
     * `` ``
     *
     * ``[ HTTP: GET /api/v1/theme/{id}/events-in-time-period ]``
     *
     * @param int $id [required] The ID of the theme.
     * @param DateTime $start [optional] The start of the time period to search. If
     * omitted, this will default to 0:00am today.
     * @param DateTime $end [optional] The end of the time period to search. If
     * omitted, this will default to the first midnight after the start date.
     * @param boolean $checkSessions [optional] If ``true``, check the event
     * sessions, and exclude any events that have no sessions overlapping the
     * the time period being searched.
     * @param string $fetch [optional] A comma-separated list of any additional
     * details to fetch for each event.
     *
     * @return UTBSEvent[] A list of events found, in (start date-time, ID) order.
     */
    public function getEventsInTimePeriod($id,
                                          $start=null,
                                          $end=null,
                                          $checkSessions=null,
                                          $fetch=null)
    {
        $pathParams = array("id" => $id);
        $queryParams = array("start"         => $start,
                             "end"           => $end,
                             "checkSessions" => $checkSessions,
                             "fetch"         => $fetch);
        $formParams = array();
        $result = $this->conn->invokeMethod("GET",
                                            'api/v1/theme/%1$s/events-in-time-period',
                                            $pathParams,
                                            $queryParams,
                                            $formParams);
        if (isset($result->error))
            throw new UTBSException($result->error);
        return $result->events;
    }

    /**
     * Get a theme's self-taught events.
     *
     * By default, only a few basic details about each event are returned,
     * but the optional ``fetch`` parameter may be used to fetch
     * additional attributes or references.
     *
     * `` ``
     *
     * ``[ HTTP: GET /api/v1/theme/{id}/self-taught-events ]``
     *
     * @param int $id [required] The ID of the theme.
     * @param string $fetch [optional] A comma-separated list of any additional
     * details to fetch for each event.
     *
     * @return UTBSEvent[] A list of self-taught events, in (title, ID) order.
     */
    public function getSelfTaughtEvents($id,
                                        $fetch=null)
    {
        $pathParams = array("id" => $id);
        $queryParams = array("fetch" => $fetch);
        $formParams = array();
        $result = $this->conn->invokeMethod("GET",
                                            'api/v1/theme/%1$s/self-taught-events',
                                            $pathParams,
                                            $queryParams,
                                            $formParams);
        if (isset($result->error))
            throw new UTBSException($result->error);
        return $result->events;
    }
}
