<?php
/*
Copyright (c) 2013, University of Cambridge Computing Service.

This file is part of the University Training Booking System client library.

This library is free software: you can redistribute it and/or modify
it under the terms of the GNU Lesser General Public License as published
by the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This library is distributed in the hope that it will be useful, but
WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Lesser General Public
License for more details.

You should have received a copy of the GNU Lesser General Public License
along with this library.  If not, see <http://www.gnu.org/licenses/>.
*/

require_once "UTBSDto.php";
require_once "UTBSEvent.php";

/**
 * Class representing a programme of events returned by the web service API.
 *
 * @author Dean Rasheed (dev-group@ucs.cam.ac.uk)
 */
class UTBSProgramme extends UTBSDto
{
    /* Properties marked as @XmlAttribute in the JAXB class */
    protected static $xmlAttrs = array("programmeId", "id", "ref");

    /* Properties marked as @XmlElement in the JAXB class */
    protected static $xmlElems = array("provider", "shortName", "title",
                                       "status", "startDate", "endDate");

    /* Properties marked as @XmlElementWrapper in the JAXB class */
    protected static $xmlArrays = array("events");

    /** @var int The unique identifier of the programme. */
    public $programmeId;

    /**
     * @var UTBSProvider The programme's provider. This will only be
     * populated if the ``fetch`` parameter included the ``"provider"``
     * option.
     */
    public $provider;

    /**
     * @var string The programme's short name (as it appears in some
     * reports).
     */
    public $shortName;

    /**
     * @var string The programme's title (as it appears in most of the UTBS).
     */
    public $title;

    /** @var string The programme's status ("published" or "completed"). */
    public $status;

    /** @var DateTime The programme's start date. */
    public $startDate;

    /** @var DateTime The programme's end date. */
    public $endDate;

    /**
     * @var UTBSEvent[] A list of the programme's events. This will only be
     * populated if the ``fetch`` parameter included the ``"events"`` option.
     */
    public $events;

    /**
     * @ignore
     * @var string An ID that can uniquely identify this programme within the
     * returned XML/JSON document. This is only used in the flattened
     * XML/JSON representation (if the "flatten" parameter is specified).
     */
    public $id;

    /**
     * @ignore
     * @var string A reference (by id) to a programme element in the XML/JSON
     * document. This is only used in the flattened XML/JSON representation
     * (if the "flatten" parameter is specified).
     */
    public $ref;

    /* Flag to prevent infinite recursion due to circular references. */
    private $unflattened;

    /**
     * @ignore
     * Create a UTBSProgramme from the attributes of an XML node.
     *
     * @param array $attrs The attributes on the XML node.
     */
    public function __construct($attrs=array())
    {
        parent::__construct($attrs);
        if (isset($this->programmeId))
            $this->programmeId = (int )$this->programmeId;
        $this->unflattened = false;
    }

    /**
     * @ignore
     * Overridden end element callback for XML parsing.
     *
     * @param string $tagname The name of the XML element.
     * @param string $data The textual value of the XML element.
     * @return void.
     */
    public function endChildElement($tagname, $data)
    {
        parent::endChildElement($tagname, $data);
        if ($tagname === "startDate" && isset($this->startDate))
            $this->startDate = new DateTime($this->startDate);
        if ($tagname === "endDate" && isset($this->endDate))
            $this->endDate = new DateTime($this->endDate);
    }

    /**
     * @ignore
     * Unflatten a single UTBSProgramme.
     *
     * @param UTBSResultEntityMap $em The mapping from IDs to entities.
     */
    public function unflatten($em)
    {
        if (isset($this->ref))
        {
            $programme = $em->getProgramme($this->ref);
            if (!$programme->unflattened)
            {
                $programme->unflattened = true;
                if (isset($programme->provider))
                    $programme->provider = $programme->provider->unflatten($em);
                UTBSEvent::unflattenEvents($em, $programme->events);
            }
            return $programme;
        }
        return $this;
    }

    /**
     * @ignore
     * Unflatten a list of UTBSProgramme objects (done in place).
     *
     * @param UTBSResultEntityMap $em The mapping from IDs to entities.
     * @param UTBSProgramme[] $programmes The programmes to unflatten.
     */
    public static function unflattenProgrammes($em, &$programmes)
    {
        if (isset($programmes))
            foreach ($programmes as $idx => $programme)
                $programmes[$idx] = $programme->unflatten($em);
    }
}
