<?php
/*
Copyright (c) 2013, University of Cambridge Computing Service.

This file is part of the University Training Booking System client library.

This library is free software: you can redistribute it and/or modify
it under the terms of the GNU Lesser General Public License as published
by the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This library is distributed in the hope that it will be useful, but
WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Lesser General Public
License for more details.

You should have received a copy of the GNU Lesser General Public License
along with this library.  If not, see <http://www.gnu.org/licenses/>.
*/

require_once "UTBSDto.php";
require_once "UTBSInstitution.php";

/**
 * Class representing a booking on an event returned by the web service API.
 *
 * @author Dean Rasheed (dev-group@ucs.cam.ac.uk)
 */
class UTBSEventBooking extends UTBSDto
{
    /* Properties marked as @XmlAttribute in the JAXB class */
    protected static $xmlAttrs = array("bookingId", "attended", "passed",
                                       "satisfied", "id", "ref");

    /* Properties marked as @XmlElement in the JAXB class */
    protected static $xmlElems = array("event", "participant", "bookedBy",
                                       "bookingTime", "status",
                                       "institution", "misStatus",
                                       "specialRequirements");

    /* Properties marked as @XmlElementWrapper in the JAXB class */
    protected static $xmlArrays = array("sessionBookings", "institutions");

    /** @var int The unique identifier of the booking. */
    public $bookingId;

    /**
     * @var UTBSEvent The event booked. Only the event ID field will be
     * populated unless the ``fetch`` parameter included the ``"event"``
     * option.
     */
    public $event;

    /**
     * @var UTBSPerson The person the booking is for. Only the CRSid field
     * will be populated unless the ``fetch`` parameter included the
     * ``"participant"`` option.
     */
    public $participant;

    /**
     * @var UTBSPerson The person who made the booking. Only the CRSid field
     * will be populated unless the ``fetch`` parameter included the
     * ``"booked_by"`` option.
     */
    public $bookedBy;

    /** @var DateTime The time that the booking was originally made. */
    public $bookingTime;

    /**
     * @var string The status of the booking (``"provisional"``,
     * ``"booked"``, ``"standby"``, ``"offered"``, ``"no response"``,
     * ``"waiting"``, ``"interested"``, ``"cancelled"``, ``"completed"`` or
     * ``"did not book"``). Confirmed bookings will have a status of
     * ``"booked"``.
     *
     * For events with optional sessions, the set of sessions booked is
     * recorded in {@link sessionBookings}.
     */
    public $status;

    /**
     * @var UTBSInstitution The participant's institution chosen at the time
     * the booking was made. This institution may no longer be the
     * participant's institution, and it may now no longer exist (i.e., it
     * may be cancelled). Only the {@link UTBSInstitution#instid instid}
     * field will be populated unless the ``fetch`` parameter included the
     * ``"institution"`` option.
     */
    public $institution;

    /**
     * @var string The staff/student status of the participant at the time
     * the booking was made. For members of the University of Cambridge, this
     * will be one of the following: ``"staff"``, ``"student"``,
     * ``"staff,student"`` or ``""``. For external participants, it will be
     * null.
     */
    public $misStatus;

    /** @var string Any special requirements the participant has. */
    public $specialRequirements;

    /**
     * @var boolean Whether or not the participant attended all the
     * non-optional sessions of the event. This will be null until attendance
     * has been recorded for all sessions. Attendance on individual sessions
     * is recorded in {@link sessionBookings}.
     */
    public $attended;

    /**
     * @var boolean Whether or not the participant passed all the
     * non-optional sessions of an assessed event. This will be null for
     * events that are not assessed. Whether or not individual sessions were
     * passed is recorded in {@link sessionBookings}.
     */
    public $passed;

    /**
     * @var boolean Whether or not the participant was satisfied with the
     * event. This will be null if user feedback was not recorded.
     */
    public $satisfied;

    /**
     * @var UTBSSessionBooking[] A list of the session booking details. This
     * records the individual sessions that were booked, as well as sessions
     * attended and (for assessed events) sessions passed. This will only be
     * populated if the ``fetch`` parameter included the
     * ``"session_bookings"`` option.
     */
    public $sessionBookings;

    /**
     * @var UTBSInstitution[] A list of all the participant's institutions,
     * as they were at the time the booking was made. This may include
     * now-cancelled institutions. This will only be populated if the
     * ``fetch`` parameter included the ``"institutions"`` option.
     */
    public $institutions;

    /**
     * @ignore
     * @var string An ID that can uniquely identify this booking within the
     * returned XML/JSON document. This is only used in the flattened
     * XML/JSON representation (if the "flatten" parameter is specified).
     */
    public $id;

    /**
     * @ignore
     * @var string A reference (by id) to a booking element in the XML/JSON
     * document. This is only used in the flattened XML/JSON representation
     * (if the "flatten" parameter is specified).
     */
    public $ref;

    /* Flag to prevent infinite recursion due to circular references. */
    private $unflattened;

    /**
     * @ignore
     * Create a UTBSEventBooking from the attributes of an XML node.
     *
     * @param array $attrs The attributes on the XML node.
     */
    public function __construct($attrs=array())
    {
        parent::__construct($attrs);
        if (isset($this->bookingId))
            $this->bookingId = (int )$this->bookingId;
        if (isset($this->attended))
            $this->attended = strcasecmp($this->attended, "true") == 0;
        if (isset($this->passed))
            $this->passed = strcasecmp($this->passed, "true") == 0;
        if (isset($this->satisfied))
            $this->satisfied = strcasecmp($this->satisfied, "true") == 0;
        $this->unflattened = false;
    }

    /**
     * @ignore
     * Overridden end element callback for XML parsing.
     *
     * @param string $tagname The name of the XML element.
     * @param string $data The textual value of the XML element.
     * @return void.
     */
    public function endChildElement($tagname, $data)
    {
        parent::endChildElement($tagname, $data);
        if ($tagname === "bookingTime" && isset($this->bookingTime))
            $this->bookingTime = new DateTime($this->bookingTime);
    }

    /**
     * @ignore
     * Unflatten a single UTBSEventBooking.
     *
     * @param UTBSResultEntityMap $em The mapping from IDs to entities.
     */
    public function unflatten($em)
    {
        if (isset($this->ref))
        {
            $booking = $em->getBooking($this->ref);
            if (!$booking->unflattened)
            {
                $booking->unflattened = true;
                if (isset($booking->event))
                    $booking->event = $booking->event->unflatten($em);
                if (isset($booking->participant))
                    $booking->participant = $booking->participant->unflatten($em);
                if (isset($booking->bookedBy))
                    $booking->bookedBy = $booking->bookedBy->unflatten($em);
                if (isset($booking->institution))
                    $booking->institution = $booking->institution->unflatten($em);
                UTBSInstitution::unflattenInstitutions($em, $booking->institutions);
            }
            return $booking;
        }
        return $this;
    }

    /**
     * @ignore
     * Unflatten a list of UTBSEventBooking objects (done in place).
     *
     * @param UTBSResultEntityMap $em The mapping from IDs to entities.
     * @param UTBSEventBooking[] $bookings The bookings to unflatten.
     */
    public static function unflattenBookings($em, &$bookings)
    {
        if (isset($bookings))
            foreach ($bookings as $idx => $booking)
                $bookings[$idx] = $booking->unflatten($em);
    }
}
