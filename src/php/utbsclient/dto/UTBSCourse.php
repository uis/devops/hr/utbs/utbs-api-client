<?php
/*
Copyright (c) 2013, University of Cambridge Computing Service.

This file is part of the University Training Booking System client library.

This library is free software: you can redistribute it and/or modify
it under the terms of the GNU Lesser General Public License as published
by the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This library is distributed in the hope that it will be useful, but
WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Lesser General Public
License for more details.

You should have received a copy of the GNU Lesser General Public License
along with this library.  If not, see <http://www.gnu.org/licenses/>.
*/

require_once "UTBSDto.php";
require_once "UTBSEvent.php";
require_once "UTBSTheme.php";

/**
 * Class representing a course returned by the web service API.
 *
 * Note that all the course details are actually on the individual events for
 * the course, and may vary each time the course is run.
 *
 * In rare cases, a course may switch provider, and so there is no provider
 * field on this class, and there is no guarantee that all the course's
 * events have the same provider.
 *
 * @author Dean Rasheed (dev-group@ucs.cam.ac.uk)
 */
class UTBSCourse extends UTBSDto
{
    /* Properties marked as @XmlAttribute in the JAXB class */
    protected static $xmlAttrs = array("courseId", "id", "ref");

    /* Properties marked as @XmlElementWrapper in the JAXB class */
    protected static $xmlArrays = array("events", "themes");

    /** @var string The unique identifier of the course. */
    public $courseId;

    /**
     * @var UTBSEvent[] A list of the course's events. This will only be
     * populated if the ``fetch`` parameter included the ``"events"`` option.
     */
    public $events;

    /**
     * @var UTBSTheme[] A list of the course's themes. This will only be
     * populated if the ``fetch`` parameter included the ``"themes"`` option.
     */
    public $themes;

    /**
     * @ignore
     * @var string An ID that can uniquely identify this course within the
     * returned XML/JSON document. This is only used in the flattened
     * XML/JSON representation (if the "flatten" parameter is specified).
     */
    public $id;

    /**
     * @ignore
     * @var string A reference (by id) to a element course in the XML/JSON
     * document. This is only used in the flattened XML/JSON representation
     * (if the "flatten" parameter is specified).
     */
    public $ref;

    /* Flag to prevent infinite recursion due to circular references. */
    private $unflattened;

    /**
     * @ignore
     * Create a UTBSCourse from the attributes of an XML node.
     *
     * @param array $attrs The attributes on the XML node.
     */
    public function __construct($attrs=array())
    {
        parent::__construct($attrs);
        $this->unflattened = false;
    }

    /**
     * @ignore
     * Unflatten a single UTBSCourse.
     *
     * @param UTBSResultEntityMap $em The mapping from IDs to entities.
     */
    public function unflatten($em)
    {
        if (isset($this->ref))
        {
            $course = $em->getCourse($this->ref);
            if (!$course->unflattened)
            {
                $course->unflattened = true;
                UTBSEvent::unflattenEvents($em, $course->events);
                UTBSTheme::unflattenThemes($em, $course->themes);
            }
            return $course;
        }
        return $this;
    }

    /**
     * @ignore
     * Unflatten a list of UTBSCourse objects (done in place).
     *
     * @param UTBSResultEntityMap $em The mapping from IDs to entities.
     * @param UTBSCourse[] $courses The courses to unflatten.
     */
    public static function unflattenCourses($em, &$courses)
    {
        if (isset($courses))
            foreach ($courses as $idx => $course)
                $courses[$idx] = $course->unflatten($em);
    }
}
