<?php
/*
Copyright (c) 2013, University of Cambridge Computing Service.

This file is part of the University Training Booking System client library.

This library is free software: you can redistribute it and/or modify
it under the terms of the GNU Lesser General Public License as published
by the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This library is distributed in the hope that it will be useful, but
WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Lesser General Public
License for more details.

You should have received a copy of the GNU Lesser General Public License
along with this library.  If not, see <http://www.gnu.org/licenses/>.
*/

require_once "UTBSCourse.php";
require_once "UTBSDto.php";
require_once "UTBSEvent.php";

/**
 * Class representing a theme returned by the web service API.
 *
 * @author Dean Rasheed (dev-group@ucs.cam.ac.uk)
 */
class UTBSTheme extends UTBSDto
{
    /* Properties marked as @XmlAttribute in the JAXB class */
    protected static $xmlAttrs = array("themeId", "id", "ref");

    /* Properties marked as @XmlElement in the JAXB class */
    protected static $xmlElems = array("provider", "shortName", "title");

    /* Properties marked as @XmlElementWrapper in the JAXB class */
    protected static $xmlArrays = array("courses", "events");

    /** @var int The unique internal identifier of the theme. */
    public $themeId;

    /**
     * @var UTBSProvider The theme's provider. This will only be populated if
     * the ``fetch`` parameter included the ``"provider"`` option.
     */
    public $provider;

    /** @var string The theme's short name (as it appears in some URLs). */
    public $shortName;

    /** @var string The theme's title (as it appears in most of the UTBS). */
    public $title;

    /**
     * @var UTBSCourse[] A list of the theme's courses. This will only be
     * populated if the ``fetch`` parameter included the ``"courses"``
     * option.
     */
    public $courses;

    /**
     * @var UTBSEvent[] A list of all the theme's events. This will only be
     * populated if the ``fetch`` parameter included the ``"events"`` option.
     */
    public $events;

    /**
     * @ignore
     * @var string An ID that can uniquely identify this theme within the
     * returned XML/JSON document. This is only used in the flattened
     * XML/JSON representation (if the "flatten" parameter is specified).
     */
    public $id;

    /**
     * @ignore
     * @var string A reference (by id) to a theme element in the XML/JSON
     * document. This is only used in the flattened XML/JSON representation
     * (if the "flatten" parameter is specified).
     */
    public $ref;

    /* Flag to prevent infinite recursion due to circular references. */
    private $unflattened;

    /**
     * @ignore
     * Create a UTBSTheme from the attributes of an XML node.
     *
     * @param array $attrs The attributes on the XML node.
     */
    public function __construct($attrs=array())
    {
        parent::__construct($attrs);
        if (isset($this->themeId))
            $this->themeId = (int )$this->themeId;
        $this->unflattened = false;
    }

    /**
     * @ignore
     * Unflatten a single UTBSTheme.
     *
     * @param UTBSResultEntityMap $em The mapping from IDs to entities.
     */
    public function unflatten($em)
    {
        if (isset($this->ref))
        {
            $theme = $em->getTheme($this->ref);
            if (!$theme->unflattened)
            {
                $theme->unflattened = true;
                if (isset($theme->provider))
                    $theme->provider = $theme->provider->unflatten($em);
                UTBSCourse::unflattenCourses($em, $theme->courses);
                UTBSEvent::unflattenEvents($em, $theme->events);
            }
            return $theme;
        }
        return $this;
    }

    /**
     * @ignore
     * Unflatten a list of UTBSTheme objects (done in place).
     *
     * @param UTBSResultEntityMap $em The mapping from IDs to entities.
     * @param UTBSTheme[] $themes The themes to unflatten.
     */
    public static function unflattenThemes($em, &$themes)
    {
        if (isset($themes))
            foreach ($themes as $idx => $theme)
                $themes[$idx] = $theme->unflatten($em);
    }
}
