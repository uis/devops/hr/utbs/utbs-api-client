<?php
/*
Copyright (c) 2013, University of Cambridge Computing Service.

This file is part of the University Training Booking System client library.

This library is free software: you can redistribute it and/or modify
it under the terms of the GNU Lesser General Public License as published
by the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This library is distributed in the hope that it will be useful, but
WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Lesser General Public
License for more details.

You should have received a copy of the GNU Lesser General Public License
along with this library.  If not, see <http://www.gnu.org/licenses/>.
*/

require_once "UTBSDto.php";

/**
 * Class representing a person returned by the web service API.
 *
 * @author Dean Rasheed (dev-group@ucs.cam.ac.uk)
 */
class UTBSPerson extends UTBSDto
{
    /* Properties marked as @XmlAttribute in the JAXB class */
    protected static $xmlAttrs = array("crsid", "id", "ref");

    /* Properties marked as @XmlElement in the JAXB class */
    protected static $xmlElems = array("name", "email", "phoneNumber");

    /** @var string The unique identifier of the person. */
    public $crsid;

    /** @var string The person's name. */
    public $name;

    /** @var string The person's email address. */
    public $email;

    /** @var string The person's phone number. */
    public $phoneNumber;

    /**
     * @ignore
     * @var string An ID that can uniquely identify this person within the
     * returned XML/JSON document. This is only used in the flattened
     * XML/JSON representation (if the "flatten" parameter is specified).
     */
    public $id;

    /**
     * @ignore
     * @var string A reference (by id) to a person element in the XML/JSON
     * document. This is only used in the flattened XML/JSON representation
     * (if the "flatten" parameter is specified).
     */
    public $ref;

    /**
     * @ignore
     * Unflatten a single UTBSPerson.
     *
     * @param UTBSResultEntityMap $em The mapping from IDs to entities.
     */
    public function unflatten($em)
    {
        if (isset($this->ref))
            return $em->getPerson($this->ref);
        return $this;
    }

    /**
     * @ignore
     * Unflatten a list of UTBSPerson objects (done in place).
     *
     * @param UTBSResultEntityMap $em The mapping from IDs to entities.
     * @param UTBSPerson[] $people The people to unflatten.
     */
    public static function unflattenPeople($em, &$people)
    {
        if (isset($people))
            foreach ($people as $idx => $person)
                $people[$idx] = $person->unflatten($em);
    }
}
