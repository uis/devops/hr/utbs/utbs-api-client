<?php
/*
Copyright (c) 2013, University of Cambridge Computing Service.

This file is part of the University Training Booking System client library.

This library is free software: you can redistribute it and/or modify
it under the terms of the GNU Lesser General Public License as published
by the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This library is distributed in the hope that it will be useful, but
WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Lesser General Public
License for more details.

You should have received a copy of the GNU Lesser General Public License
along with this library.  If not, see <http://www.gnu.org/licenses/>.
*/

require_once "UTBSDto.php";
require_once "UTBSProgramme.php";
require_once "UTBSTheme.php";
require_once "UTBSVenue.php";

/**
 * Class representing a training provider returned by the web service API.
 *
 * @author Dean Rasheed (dev-group@ucs.cam.ac.uk)
 */
class UTBSProvider extends UTBSDto
{
    /* Properties marked as @XmlAttribute in the JAXB class */
    protected static $xmlAttrs = array("id", "ref");

    /* Properties marked as @XmlElement in the JAXB class */
    protected static $xmlElems = array("shortName", "name", "title",
                                       "institution", "urlPrefix",
                                       "contactEmail", "phoneNumber",
                                       "homePage", "venueInfo",
                                       "currentProgramme");

    /* Properties marked as @XmlElementWrapper in the JAXB class */
    protected static $xmlArrays = array("programmes", "themes", "venues");

    /** @var string The provider's unique short name. */
    public $shortName;

    /**
     * @var string The provider's full name (as it appears in the provider
     * selection menu of the UTBS).
     */
    public $name;

    /**
     * @var string The provider's title (as it appears in the title bar of
     * the UTBS).
     */
    public $title;

    /**
     * @var UTBSInstitution The institution that owns the provider. Only the
     * {@link UTBSInstitution#instid instid} field will be populated unless
     * the ``fetch`` parameter included the ``"institution"`` option.
     */
    public $institution;

    /**
     * @var string The unique URL prefix used for the provider's pages in the
     * UTBS.
     */
    public $urlPrefix;

    /** @var string The provider's contact email address. */
    public $contactEmail;

    /** @var string The provider's contact phone number. */
    public $phoneNumber;

    /** @var string The Wiki text for the provider's main home page. */
    public $homePage;

    /**
     * @var string The Wiki text describing the venues belonging to the
     * provider.
     */
    public $venueInfo;

    /**
     * @var UTBSProgramme The provider's current programme. This will only be
     * populated if the ``fetch`` parameter included the
     * ``"current_programme"`` option.
     */
    public $currentProgramme;

    /**
     * @var UTBSProgramme[] A list of all the provider's programmes. This
     * will only be populated if the ``fetch`` parameter included the
     * ``"programmes"`` option.
     */
    public $programmes;

    /**
     * @var UTBSTheme[] A list of all the themes belonging to the provider.
     * This will only be populated if the ``fetch`` parameter included the
     * ``"themes"`` option.
     */
    public $themes;

    /**
     * @var UTBSVenue[] A list of all the venues belonging to the provider.
     * This will only be populated if the ``fetch`` parameter included the
     * ``"venues"`` option.
     */
    public $venues;

    /**
     * @ignore
     * @var string An ID that can uniquely identify this provider within the
     * returned XML/JSON document. This is only used in the flattened
     * XML/JSON representation (if the "flatten" parameter is specified).
     */
    public $id;

    /**
     * @ignore
     * @var string A reference (by id) to a provider element in the XML/JSON
     * document. This is only used in the flattened XML/JSON representation
     * (if the "flatten" parameter is specified).
     */
    public $ref;

    /* Flag to prevent infinite recursion due to circular references. */
    private $unflattened;

    /**
     * @ignore
     * Create a UTBSProvider from the attributes of an XML node.
     *
     * @param array $attrs The attributes on the XML node.
     */
    public function __construct($attrs=array())
    {
        parent::__construct($attrs);
        $this->unflattened = false;
    }

    /**
     * @ignore
     * Unflatten a single UTBSProvider.
     *
     * @param UTBSResultEntityMap $em The mapping from IDs to entities.
     */
    public function unflatten($em)
    {
        if (isset($this->ref))
        {
            $provider = $em->getProvider($this->ref);
            if (!$provider->unflattened)
            {
                $provider->unflattened = true;
                if (isset($provider->institution))
                    $provider->institution = $provider->institution->unflatten($em);
                if (isset($provider->currentProgramme))
                    $provider->currentProgramme = $provider->currentProgramme->unflatten($em);
                UTBSProgramme::unflattenProgrammes($em, $provider->programmes);
                UTBSTheme::unflattenThemes($em, $provider->themes);
                UTBSVenue::unflattenVenues($em, $provider->venues);
            }
            return $provider;
        }
        return $this;
    }

    /**
     * @ignore
     * Unflatten a list of UTBSProvider objects (done in place).
     *
     * @param UTBSResultEntityMap $em The mapping from IDs to entities.
     * @param UTBSProvider[] $providers The providers to unflatten.
     */
    public static function unflattenProviders($em, &$providers)
    {
        if (isset($providers))
            foreach ($providers as $idx => $provider)
                $providers[$idx] = $provider->unflatten($em);
    }
}
