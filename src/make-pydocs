#!/bin/bash

# -------------------------------------------------------------------------
# Make the Python documentation using Sphinx.
#
# Usage: make-pydocs <python-version> <app-version>
#
# The parameter <python-version> should be "2.7" or "3.4". This is the
# version of the online Pydocs that we link to.
# -------------------------------------------------------------------------

DIRNAME=$(cd $(dirname "$0") ; pwd -P)

# Get the target Python version
PYTHON_VERSION=$1
if [ "$PYTHON_VERSION" == "" ] ; then
    echo "Usage: make-pydocs <python-version> <app-version>"
    exit 1
fi
case "$PYTHON_VERSION" in
  2.7)
    CODE_SRCDIR=$(realpath $DIRNAME/python/utbsclient)
    PYTHON=$(which python)
    ;;
  3.4)
    CODE_SRCDIR=$(realpath $DIRNAME/python3/utbsclient)
    PYTHON=$(which python3)
    ;;
  *)
    echo "Unsupported Python version: $PYTHON_VERSION"
    exit 1
esac

# Get the application version
APP_VERSION=$2
if [ "$APP_VERSION" == "" ] ; then
    echo "Usage: make-pydocs <python-version> <app-version>"
    exit 1
fi

# Common Python source documentation and configuration
DOC_SRCDIR=$(realpath $DIRNAME/../doc/pydocs)

# Documentation build directory
BUILDDIR=$(realpath $DIRNAME/../build/pydocs-$PYTHON_VERSION)
mkdir -p $BUILDDIR

# Setup a virtualenv environment in this directory, with Sphinx and
# sphinxcontrib-fulltoc installed
cd $BUILDDIR
$PYTHON -m venv env
source env/bin/activate
pip install Sphinx
pip install sphinxcontrib-fulltoc

# Copy the relevant Python source code to the build directory, preserving
# its timestamps
rm -rf $BUILDDIR/utbsclient
cp -pr $CODE_SRCDIR $BUILDDIR

# Similiarly copy the common Python source documentation and configuration to
# the build directory, but keep any old files from previous builds so that we
# can preserve the timestamps of unchanged files
if [ -d $BUILDDIR/docs ] ; then
    rm -rf $BUILDDIR/docs.old
    mv $BUILDDIR/docs $BUILDDIR/docs.old
fi
cp -pr $DOC_SRCDIR $BUILDDIR/docs

# Insert the requested application and Python version numbers into the copy
# of the Sphinx configuration
sed -i "s/@app.version@/$APP_VERSION/" $BUILDDIR/docs/conf.py
sed -i "s/@python.version@/$PYTHON_VERSION/" $BUILDDIR/docs/conf.py

# Function to find all Python classes in one of the modules to be documented
function get_classes_in_module
{
    local MODULE=$1
    pushd $BUILDDIR >/dev/null
    $PYTHON <<EOF
import inspect
from utbsclient import *

for name, obj in inspect.getmembers($MODULE):
    if inspect.isclass(obj) and\
       str(obj).find('utbsclient.$MODULE') != -1:
        print(name)
EOF
    popd >/dev/null
}

# Create documentation source files for each class to be documented, using
# Sphinx's autodoc extension. Also, while we're collecting class information,
# build the table of contents for each module.
for FILE in `ls $BUILDDIR/utbsclient/*.py | grep -v __init__.py` ; do
    MODULE=$(basename -s .py $FILE)
    MODULE_TOC=".. toctree::\n"

    for CLASS in `get_classes_in_module $MODULE` ; do
        TITLE="$CLASS"
        UNDERLINE=$(echo "$TITLE" | sed -e "s/./=/g")
        MODULE_TOC="$MODULE_TOC\n    $TITLE <utbsclient.$MODULE.$CLASS>"

        # Class documentation, with a Sphinx autodoc directive to generate
        # the documentation for this class
        cat <<EOF > $BUILDDIR/docs/utbsclient.$MODULE.$CLASS.rst
$TITLE
$UNDERLINE

Class in module :mod:\`utbsclient.$MODULE\`

.. autoclass:: utbsclient.$MODULE.$CLASS
    :members:
    :undoc-members:
    :show-inheritance:
EOF
    done

    # Insert the module's table of contents (list of classes) into the
    # module documentation
    sed -i "s/@module.toc@/$MODULE_TOC/" $BUILDDIR/docs/utbsclient.$MODULE.rst
done

# If we had some previous source documentation and configuration files,
# restore their timestamps so that Sphinx doesn't do a rebuild when nothing
# has changed (Sphinx rebuilds under Python 3 produce slightly different
# results each time)
if [ -d $BUILDDIR/docs.old ] ; then
    rsync -cru --delete $BUILDDIR/docs/ $BUILDDIR/docs.old/
    rm -rf $BUILDDIR/docs
    mv $BUILDDIR/docs.old $BUILDDIR/docs
fi

# Finally, use Sphinx to actually build everything. For the python3 docs, use
# a modified version of sphinx-build, using the python3 interpreter.
mkdir -p $BUILDDIR/html
SPHINX_BUILD=$(which sphinx-build)

if [[ $PYTHON_VERSION =~ ^3 ]] ; then
    cp $SPHINX_BUILD $BUILDDIR/sphinx3-build
    SPHINX_BUILD=$BUILDDIR/sphinx3-build
    sed -i "s/^#!\/usr\/bin\/python$/#!\/usr\/bin\/python3/" $SPHINX_BUILD
    chmod 755 $SPHINX_BUILD
fi
$SPHINX_BUILD -b html $BUILDDIR/docs $BUILDDIR/html

deactivate

echo ""
echo "DONE - Output written to $BUILDDIR/html/index.html"
