# === AUTO-GENERATED - DO NOT EDIT ===

# --------------------------------------------------------------------------
# Copyright (c) 2013, University of Cambridge Computing Service.
#
# This file is part of the University Training Booking System client library.
#
# This library is free software: you can redistribute it and/or modify
# it under the terms of the GNU Lesser General Public License as published
# by the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This library is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
# or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Lesser General Public
# License for more details.
#
# You should have received a copy of the GNU Lesser General Public License
# along with this library.  If not, see <http://www.gnu.org/licenses/>.
# --------------------------------------------------------------------------

"""
Web service API methods. This module is fully auto-generated, and contains
the Python equivalent of the `XxxMethods` Java classes for executing all API
methods.
"""

from .connection import UTBSException


class UTBSMethods:
    """
    Common methods for searching for objects in the UTBS database.

    .. codeauthor:: Dean Rasheed (dev-group@ucs.cam.ac.uk)
    """
    def __init__(self, conn):
        self.conn = conn

    def getCurrentUser(self):
        """
        Get the currently authenticated API user.

        ``[ HTTP: GET /api/v1/current-user ]``

        **Returns**
          str
            The API user, or null if authentication details have not been
            supplied.
        """
        path = "api/v1/current-user"
        path_params = {}
        query_params = {}
        form_params = {}
        result = self.conn.invoke_method("GET", path, path_params,
                                         query_params, form_params)
        if result.error:
            raise UTBSException(result.error)
        return result.value

    def getPrivileges(self,
                      provider=None):
        """
        Get full list of privileges that the currently authenticated user has,
        including role privileges.

        If a provider is not specified, this will only include privileges
        granted to all providers. Otherwise it will include privileges granted
        for the specified provider, in addition to all-provider privileges.

        ``[ HTTP: GET /api/v1/privileges ]``

        **Parameters**
          `provider` : str
            [optional] The short name of a provider.

        **Returns**
          str
            The user's privileges (separated by newlines).
        """
        path = "api/v1/privileges"
        path_params = {}
        query_params = {"provider": provider}
        form_params = {}
        result = self.conn.invoke_method("GET", path, path_params,
                                         query_params, form_params)
        if result.error:
            raise UTBSException(result.error)
        return result.value

    def getVersion(self):
        """
        Get the current API version number.

        ``[ HTTP: GET /api/v1/version ]``

        **Returns**
          str
            The API version number string.
        """
        path = "api/v1/version"
        path_params = {}
        query_params = {}
        form_params = {}
        result = self.conn.invoke_method("GET", path, path_params,
                                         query_params, form_params)
        if result.error:
            raise UTBSException(result.error)
        return result.value


class BookingMethods:
    """
    Methods for querying and manipulating bookings.

    **The fetch parameter for event bookings**

    All methods that return bookings also accept an optional
    `fetch` parameter that may be used to request additional
    information about the bookings returned. For more details about the
    general rules that apply to the `fetch` parameter, refer to the
    :any:`EventMethods` documentation.

    For bookings the `fetch` parameter may be used to fetch details
    about the individual sessions booked and the referenced event, people and
    institutions:

    * ``"event"`` - fetches details about the event booked.

    * ``"participant"`` - fetches details about the person the booking
      is for.

    * ``"booked_by"`` - fetches details about the person who made the
      booking.

    * ``"institution"`` - fetches details about the participant's
      institution chosen at the time the booking was made.

    * ``"session_bookings"`` - fetches the session booking details.

    * ``"institutions"`` - fetches a list of all the participant's
      institutions, as they were at the time the booking was made.

    As with the event `fetch` parameter, this reference may be used
    in a chain by using the "dot" notation to fetch additional information
    about the referenced event or institutions. For example
    "institution.school" will fetch the participant's chosen institution and
    the school to which it belongs. For more information about what can be
    fetched from the referenced event or institutions, refer to the
    documentation for :any:`EventMethods` and :any:`InstitutionMethods`.

    .. codeauthor:: Dean Rasheed (dev-group@ucs.cam.ac.uk)
    """
    def __init__(self, conn):
        self.conn = conn

    def getBooking(self,
                   id,
                   fetch=None):
        """
        Get the event booking with the specified ID.

        By default, only a few basic details about the booking are returned,
        but the optional `fetch` parameter may be used to fetch
        additional attributes or references.

        Note that viewing bookings requires authentication, and a booking is
        only visible to the following:

        * The participant.

        * The person who made the booking (not necessarily the
          participant).

        * The event's trainers.

        * The event's administrator, owner and booker.

        * People with the "view-event-bookings" privilege for the event's
          provider.

        * People with the "view-3rd-party-booking" privilege for the event's
          provider.

        ``[ HTTP: GET /api/v1/booking/{id} ]``

        **Parameters**
          `id` : long
            [required] The ID of the booking to fetch.

          `fetch` : str
            [optional] A comma-separated list of any additional
            details to fetch.

        **Returns**
          :any:`UTBSEventBooking`
            The requested booking or null if it was not found.
        """
        path = "api/v1/booking/%(id)s"
        path_params = {"id": id}
        query_params = {"fetch": fetch}
        form_params = {}
        result = self.conn.invoke_method("GET", path, path_params,
                                         query_params, form_params)
        if result.error:
            raise UTBSException(result.error)
        return result.booking

    def updateAttendance(self,
                         id,
                         sessionNumber=None,
                         attended=None):
        """
        Update an event booking to record whether or not the participant
        attended the event.

        The attendance may be updated for a single session on the event, by
        specifying a particular session number, or for all booked sessions, by
        passing :any:`None` as the session number.

        The attendance may be set to :any:`True` or :any:`False` to indicate
        whether or not the participant attended, or it may be set to
        :any:`None` (its original value) to indicate that their attendance
        has not yet been recorded, or is currently unknown.

        Note that updating attendance requires authentication as a user with
        permission to view the booking (see
        :any:`getBooking() <BookingMethods.getBooking(Long,String)>`) and record attendance
        on the event. So in addition to being able to view the booking, the
        user must be either one of the following:

        * The event administrator.

        * A person with the "record-attendance" privilege for the event's
          provider.

        ``[ HTTP: PUT /api/v1/booking/{id}/attendance ]``

        **Parameters**
          `id` : long
            [required] The ID of the booking to update.

          `sessionNumber` : int
            [optional] The session to update the attendance
            for, or :any:`None` to update all booked sessions (the default).

          `attended` : bool
            [optional] Whether or not the participant attended, or
            :any:`None` to record that their attendance has not been set (the
            default).

        **Returns**
          :any:`UTBSEventBooking`
            The updated booking, together with all associated session
            bookings.
        """
        path = "api/v1/booking/%(id)s/attendance"
        path_params = {"id": id}
        query_params = {}
        form_params = {"sessionNumber": sessionNumber,
                       "attended": attended}
        result = self.conn.invoke_method("PUT", path, path_params,
                                         query_params, form_params)
        if result.error:
            raise UTBSException(result.error)
        return result.booking

    def updateOutcome(self,
                      id,
                      sessionNumber=None,
                      attended=None,
                      passed=None):
        """
        Update an event booking to record whether or not the participant
        attended the event, and whether they passed or failed.

        The attendance and outcome may be updated for a single session on the
        event, by specifying a particular session number, or for all booked
        sessions, by passing :any:`None` as the session number.

        The attendance and outcome may each be set to :any:`True` or
        :any:`False` to indicate whether or not the participant attended or
        passed, or it may be set to :any:`None` (its original value) to
        indicate that their attendance/outcome has not yet been recorded, or
        is currently unknown.

        Note that updating attendance/outcome requires authentication as a user
        with permission to view the booking (see
        :any:`getBooking() <BookingMethods.getBooking(Long,String)>`) and record attendance
        on the event. So in addition to being able to view the booking, the
        user must be either one of the following:

        * The event administrator.

        * A person with the "record-attendance" privilege for the event's
          provider.

        ``[ HTTP: PUT /api/v1/booking/{id}/outcome ]``

        **Parameters**
          `id` : long
            [required] The ID of the booking to update.

          `sessionNumber` : int
            [optional] The session to update the attendance
            for, or :any:`None` to update all booked sessions (the default).

          `attended` : bool
            [optional] Whether or not the participant attended, or
            :any:`None` to record that their attendance has not been set (the
            default).

          `passed` : bool
            [optional] Whether or not the participant passed, or
            :any:`None` to record that their outcome has not been set (the
            default).

        **Returns**
          :any:`UTBSEventBooking`
            The updated booking, together with all associated session
            bookings.
        """
        path = "api/v1/booking/%(id)s/outcome"
        path_params = {"id": id}
        query_params = {}
        form_params = {"sessionNumber": sessionNumber,
                       "attended": attended,
                       "passed": passed}
        result = self.conn.invoke_method("PUT", path, path_params,
                                         query_params, form_params)
        if result.error:
            raise UTBSException(result.error)
        return result.booking


class CourseMethods:
    """
    Methods for querying and manipulating courses.

    Note that all the course details are actually on the individual events for
    the course, and may vary each time the course is run, so most of the
    useful methods for querying course details are actually in
    :any:`EventMethods`.

    **The fetch parameter for courses**

    All methods that return courses also accept an optional `fetch`
    parameter that may be used to request additional information about the
    courses returned. For more details about the general rules that apply to
    the `fetch` parameter, refer to the :any:`EventMethods`
    documentation.

    For courses the `fetch` parameter may be used to fetch
    referenced events and themes. The following references are
    supported:

    * ``"events"`` - fetches all the course's events, in (date, ID)
      order.

    * ``"themes"`` - fetches all the themes containing the course.

    As with the event `fetch` parameter, the references may be used
    in a chain by using the "dot" notation to fetch additional information
    about referenced events and themes. For example "events.sessions" will
    fetch all the course's events and all their sessions. For more information
    about what can be fetched from referenced events and themes, refer to the
    documentation for :any:`EventMethods` and :any:`ThemeMethods`.

    .. codeauthor:: Dean Rasheed (dev-group@ucs.cam.ac.uk)
    """
    def __init__(self, conn):
        self.conn = conn

    def getCourse(self,
                  id,
                  fetch=None):
        """
        Get the course with the specified ID.

        Note that a course by itself has very few attributes, since all the
        interesting details are held on the events in the course, so typically
        the optional `fetch` parameter should be used to fetch the
        course's events.

        The `fetch` parameter may also be used to fetch the themes
        that contain the course.

        ``[ HTTP: GET /api/v1/course/{id} ]``

        **Parameters**
          `id` : str
            [required] The ID of the course to fetch.

          `fetch` : str
            [optional] A comma-separated list of any additional
            details to fetch.

        **Returns**
          :any:`UTBSCourse`
            The requested course or null if it was not found.
        """
        path = "api/v1/course/%(id)s"
        path_params = {"id": id}
        query_params = {"fetch": fetch}
        form_params = {}
        result = self.conn.invoke_method("GET", path, path_params,
                                         query_params, form_params)
        if result.error:
            raise UTBSException(result.error)
        return result.course

    def getEventsInTimePeriod(self,
                              id,
                              start=None,
                              end=None,
                              checkSessions=None,
                              fetch=None):
        """
        Get the events for the specified course in the specified time period.

        This will return any events that overlap the specified time period.
        More specifically, it will return events whose start is less than or
        equal to the end of the time period, and whose end is greater than or
        equal to the start of the time period (i.e., all the start and end
        timestamps are treated inclusively).

        Optionally, this will also check the event's sessions and exclude any
        events that have no sessions overlapping the specified time period.
        This can happen for events with multiple sessions. For example,
        suppose an event has 2 sessions, one on Monday and the other on
        Friday, and that the specified time period to search was on Wednesday.
        Then by default, the event would be returned, because it starts on
        Monday and ends on Friday, which overlaps the time period being
        searched, but if session checking is enabled, the event would be
        excluded.

        By default, only a few basic details about each event are returned,
        but the optional `fetch` parameter may be used to fetch
        additional attributes or references.

        .. note::
          When using this API directly via the URL endpoints, date-time
          parameters should be supplied as either milliseconds since epoch, or
          as ISO 8601 formatted date or date-time strings.

        ``[ HTTP: GET /api/v1/course/{id}/events-in-time-period ]``

        **Parameters**
          `id` : str
            [required] The ID of the course.

          `start` : datetime
            [optional] The start of the time period to search. If
            omitted, this will default to 0:00am today.

          `end` : datetime
            [optional] The end of the time period to search. If
            omitted, this will default to the first midnight after the start date.

          `checkSessions` : bool
            [optional] If :any:`True`, check the event
            sessions, and exclude any events that have no sessions overlapping the
            the time period being searched.

          `fetch` : str
            [optional] A comma-separated list of any additional
            details to fetch for each event.

        **Returns**
          list of :any:`UTBSEvent`
            A list of events found, in (start date-time, ID) order.
        """
        path = "api/v1/course/%(id)s/events-in-time-period"
        path_params = {"id": id}
        query_params = {"start": start,
                        "end": end,
                        "checkSessions": checkSessions,
                        "fetch": fetch}
        form_params = {}
        result = self.conn.invoke_method("GET", path, path_params,
                                         query_params, form_params)
        if result.error:
            raise UTBSException(result.error)
        return result.events

    def getSelfTaughtEvents(self,
                            id,
                            fetch=None):
        """
        Get a course's self-taught events.

        By default, only a few basic details about each event are returned,
        but the optional `fetch` parameter may be used to fetch
        additional attributes or references.

        ``[ HTTP: GET /api/v1/course/{id}/self-taught-events ]``

        **Parameters**
          `id` : str
            [required] The ID of the course.

          `fetch` : str
            [optional] A comma-separated list of any additional
            details to fetch for each event.

        **Returns**
          list of :any:`UTBSEvent`
            A list of self-taught events, in (title, ID) order.
        """
        path = "api/v1/course/%(id)s/self-taught-events"
        path_params = {"id": id}
        query_params = {"fetch": fetch}
        form_params = {}
        result = self.conn.invoke_method("GET", path, path_params,
                                         query_params, form_params)
        if result.error:
            raise UTBSException(result.error)
        return result.events


class EventMethods:
    """
    Methods for querying and manipulating events.

    **The fetch parameter for events**

    All methods that return events also accept an optional `fetch`
    parameter that may be used to request additional information about the
    events returned. The following additional information may be fetched:

    * ``"provider"`` - fetches the event's provider.

    * ``"programme"`` - fetches the programme containing the event.

    * ``"course"`` - fetches the event's course, from which other events
      for the same course may be fetched by specifying
      ``"course.events"``.

    * ``"topics"`` - fetches the event's topics sections.

    * ``"extra_info"`` - fetches any extra information sections that the
      event has.

    * ``"related_courses"`` - fetches any related courses for the event,
      from which related events may be fetched by specifying
      ``"related_courses.events"``.

    * ``"themes"`` - fetches the event's themes.

    * ``"sessions"`` - fetches the event's sessions.

    * ``"sessions.trainers"`` - fetches the event's sessions and the
      trainers for each session.

    * ``"sessions.venue"`` - fetches the event's sessions and the venue
      for each session.

    * ``"trainers"`` - fetches the event's trainers, but not the
      individual sessions that they are assigned to.

    * ``"venues"`` - fetches the event's venues, but not the individual
      sessions held in each venue.

    * ``"bookings"`` - fetches the event's bookings, if the user is
      authenticated and has the appropriate privileges for the event. This may
      be an incomplete list, if the user does not have permission to view all
      the event's bookings.

    **Additional notes on the fetch parameter**

    In addition to the above values, the `fetch` parameter may also
    be used in a chain using "dot" notation to fetch additional information
    about each entity returned. For example:

    ``fetch = "themes.events"``
    This fetches the events's themes, and also all the events for each of
    those themes.

    ``fetch = "programme.events.sessions"``
    This fetches the event's programme, all the events in that programme and
    all the sessions in each of those events.

    For more information about what may be fetched from each entity type in
    such a chain of fetches, refer to the documentation for the relevent
    entity type's methods: :any:`ProviderMethods`, :any:`ProgrammeMethods`,
    :any:`ThemeMethods`, :any:`CourseMethods`, :any:`VenueMethods`.

    The `fetch` parameter also supports fetching multiple different
    kinds of additional information, by specifying a comma-separated list of
    values, for example:

    ``fetch = "course.events.programme,themes.events.trainers"``
    This fetches all the events for the same course, and their programmes,
    plus all the events in the same theme(s) as this event, together with
    their trainers.

    .. codeauthor:: Dean Rasheed (dev-group@ucs.cam.ac.uk)
    """
    def __init__(self, conn):
        self.conn = conn

    def getEventsInTimePeriod(self,
                              start=None,
                              end=None,
                              checkSessions=None,
                              fetch=None):
        """
        Get all events in the specified time period.

        This will return any events that overlap the specified time period.
        More specifically, it will return events whose start is less than or
        equal to the end of the time period, and whose end is greater than or
        equal to the start of the time period (i.e., all the start and end
        timestamps are treated inclusively).

        Optionally, this will also check the event's sessions and exclude any
        events that have no sessions overlapping the specified time period.
        This can happen for events with multiple sessions. For example,
        suppose an event has 2 sessions, one on Monday and the other on
        Friday, and that the specified time period to search was on Wednesday.
        Then by default, the event would be returned, because it starts on
        Monday and ends on Friday, which overlaps the time period being
        searched, but if session checking is enabled, the event would be
        excluded.

        By default, only a few basic details about each event are returned,
        but the optional `fetch` parameter may be used to fetch
        additional attributes or references.

        .. note::
          When using this API directly via the URL endpoints, date-time
          parameters should be supplied as either milliseconds since epoch, or
          as ISO 8601 formatted date or date-time strings.

        ``[ HTTP: GET /api/v1/event/events-in-time-period ]``

        **Parameters**
          `start` : datetime
            [optional] The start of the time period to search. If
            omitted, this will default to 0:00am today.

          `end` : datetime
            [optional] The end of the time period to search. If
            omitted, this will default to the first midnight after the start date.

          `checkSessions` : bool
            [optional] If :any:`True`, check the event
            sessions, and exclude any events that have no sessions overlapping the
            the time period being searched.

          `fetch` : str
            [optional] A comma-separated list of any additional
            details to fetch for each event.

        **Returns**
          list of :any:`UTBSEvent`
            A list of events found, in (start date-time, ID) order.
        """
        path = "api/v1/event/events-in-time-period"
        path_params = {}
        query_params = {"start": start,
                        "end": end,
                        "checkSessions": checkSessions,
                        "fetch": fetch}
        form_params = {}
        result = self.conn.invoke_method("GET", path, path_params,
                                         query_params, form_params)
        if result.error:
            raise UTBSException(result.error)
        return result.events

    def getEvent(self,
                 id,
                 fetch=None):
        """
        Get the event with the specified ID.

        By default, only a few basic details about the event are returned, but
        the optional `fetch` parameter may be used to fetch
        additional details.

        ``[ HTTP: GET /api/v1/event/{id} ]``

        **Parameters**
          `id` : long
            [required] The ID of the event to fetch.

          `fetch` : str
            [optional] A comma-separated list of any additional
            details to fetch.

        **Returns**
          :any:`UTBSEvent`
            The requested event or null if it was not found.
        """
        path = "api/v1/event/%(id)s"
        path_params = {"id": id}
        query_params = {"fetch": fetch}
        form_params = {}
        result = self.conn.invoke_method("GET", path, path_params,
                                         query_params, form_params)
        if result.error:
            raise UTBSException(result.error)
        return result.event

    def getBookingForCrsid(self,
                           id,
                           crsid,
                           fetch=None):
        """
        Get the booking (if there is one) for the specified participant on this
        event.

        By default, only a few basic details about each booking are returned,
        but the optional `fetch` parameter may be used to fetch
        additional details about each booking.

        .. note::
          This will return cancelled bookings, waiting list places, etc.
          Check the booking's :any:`status <UTBSEventBooking#status>` field to see
          if it is a confirmed bookings.

        .. note::
          It is normally only possible for a participant to have one
          booking on an event.  An exception to this is a booking with a status
          of 'interested', which represents an expression of interest in the
          course as a whole, rather than this specific event.  If a participant
          has a regular booking and an 'interested' booking, this method will
          return the regular one only.  If they have only an 'interested'
          booking, then it will be returned.  So always check the booking's
          :any:`status <UTBSEventBooking#status>` field.

        Note that viewing an event's bookings requires authentication as one
        of the following:

        * One of the event's trainers.

        * The event's administrator, owner or booker.

        * A person with the "view-event-bookings" privilege for the event's
          provider.

        * A person with the "view-3rd-party-booking" privilege for the
          event's provider.

        ``[ HTTP: GET /api/v1/event/{id}/bookingForCrsid/{crsid} ]``

        **Parameters**
          `id` : long
            [required] The ID of the event.

          `crsid` : str
            [required] The CRSid of the participant.

          `fetch` : str
            [optional] A comma-separated list of any additional
            details to fetch for each booking.

        **Returns**
          :any:`UTBSEventBooking`
            The participant's booking on this event, if they have one.
        """
        path = "api/v1/event/%(id)s/bookingForCrsid/%(crsid)s"
        path_params = {"id": id,
                       "crsid": crsid}
        query_params = {"fetch": fetch}
        form_params = {}
        result = self.conn.invoke_method("GET", path, path_params,
                                         query_params, form_params)
        if result.error:
            raise UTBSException(result.error)
        return result.booking

    def getBookingForEmail(self,
                           id,
                           email,
                           fetch=None):
        """
        Get the booking (if there is one) for the specified participant on this
        event.

        By default, only a few basic details about each booking are returned,
        but the optional `fetch` parameter may be used to fetch
        additional details about each booking.

        .. note::
          This will return cancelled bookings, waiting list places, etc.
          Check the booking's :any:`status <UTBSEventBooking#status>` field to see
          if it is a confirmed bookings.

        .. note::
          It is normally only possible for a participant to have one
          booking on an event.  An exception to this is a booking with a status
          of 'interested', which represents an expression of interest in the
          course as a whole, rather than this specific event.  If a participant
          has a regular booking and an 'interested' booking, this method will
          return the regular one only.  If they have only an 'interested'
          booking, then it will be returned.  So always check the booking's
          :any:`status <UTBSEventBooking#status>` field.

        Note that viewing an event's bookings requires authentication as one
        of the following:

        * One of the event's trainers.

        * The event's administrator, owner or booker.

        * A person with the "view-event-bookings" privilege for the event's
          provider.

        * A person with the "view-3rd-party-booking" privilege for the
          event's provider.

        ``[ HTTP: GET /api/v1/event/{id}/bookingForEmail/{email} ]``

        **Parameters**
          `id` : long
            [required] The ID of the event.

          `email` : str
            [required] The email address of the participant.

          `fetch` : str
            [optional] A comma-separated list of any additional
            details to fetch for each booking.

        **Returns**
          :any:`UTBSEventBooking`
            The participant's booking on this event, if they have one.
        """
        path = "api/v1/event/%(id)s/bookingForEmail/%(email)s"
        path_params = {"id": id,
                       "email": email}
        query_params = {"fetch": fetch}
        form_params = {}
        result = self.conn.invoke_method("GET", path, path_params,
                                         query_params, form_params)
        if result.error:
            raise UTBSException(result.error)
        return result.booking

    def getBookings(self,
                    id,
                    fetch=None):
        """
        Get the bookings for the specified event.

        By default, only a few basic details about each booking are returned,
        but the optional `fetch` parameter may be used to fetch
        additional details about each booking.

        .. note::
          This will return **all** bookings, including cancelled
          bookings. Check each booking's :any:`status <UTBSEventBooking#status>`
          field to see which are confirmed bookings.

        Note that viewing an event's bookings requires authentication as one
        of the following:

        * One of the event's trainers.

        * The event's administrator, owner or booker.

        * A person with the "view-event-bookings" privilege for the event's
          provider.

        * A person with the "view-3rd-party-booking" privilege for the
          event's provider.

        ``[ HTTP: GET /api/v1/event/{id}/bookings ]``

        **Parameters**
          `id` : long
            [required] The ID of the event.

          `fetch` : str
            [optional] A comma-separated list of any additional
            details to fetch for each booking.

        **Returns**
          list of :any:`UTBSEventBooking`
            The event's bookings, in the order they were created.
        """
        path = "api/v1/event/%(id)s/bookings"
        path_params = {"id": id}
        query_params = {"fetch": fetch}
        form_params = {}
        result = self.conn.invoke_method("GET", path, path_params,
                                         query_params, form_params)
        if result.error:
            raise UTBSException(result.error)
        return result.bookings

    def recordAttendance(self,
                         id,
                         sessionNumber,
                         crsid,
                         autoCreateBooking=None):
        """
        Update the attendance record for the specified event, recording the
        fact that the specified person attended the specified session.

        Optionally, this will create a booking record for the participant, if
        they do not already have one. If a booking is created in this way, its
        status will be "did not book", to indicate that the participant did
        not book onto the event themselves.

        Note that this requires authentication as a user with permission to
        view the event's bookings and record attendance on the event.
        Additionally, the "create-3rd-party-booking" privilege is required if
        `autoCreateBooking` is set.

        ``[ HTTP: PUT /api/v1/event/{id}/{sessionNumber}/attendance ]``

        **Parameters**
          `id` : long
            [required] The ID of the event.

          `sessionNumber` : int
            [required] The session to record attendance for.

          `crsid` : str
            [required] The CRSid of the person who attended.

          `autoCreateBooking` : bool
            [optional] If :any:`True`, automatically
            create a booking for the person, if they do not already have one.
            Otherwise, an error will be raised if the person has not booked.

        **Returns**
          :any:`UTBSEventBooking`
            The existing or newly created booking record for the person,
            together with all associated session bookings.
        """
        path = "api/v1/event/%(id)s/%(sessionNumber)s/attendance"
        path_params = {"id": id,
                       "sessionNumber": sessionNumber}
        query_params = {}
        form_params = {"crsid": crsid,
                       "autoCreateBooking": autoCreateBooking}
        result = self.conn.invoke_method("PUT", path, path_params,
                                         query_params, form_params)
        if result.error:
            raise UTBSException(result.error)
        return result.booking


class InstitutionMethods:
    """
    Methods for querying and manipulating institutions.

    **The fetch parameter for institutions**

    All methods that return institutions also accept an optional
    `fetch` parameter that may be used to request additional
    information about the institutions returned. For more details about the
    general rules that apply to the  `fetch` parameter, refer to the
    :any:`EventMethods` documentation.

    For institutions the `fetch` parameter may be used to fetch
    referenced institutions. The following references are supported:

    * ``"parent"`` - fetches the institution's parent institution.

    * ``"school"`` - fetches the institution's school institution.

    * ``"children"`` - fetches all the institution's child institutions.
      This will exclude cancelled children, unless the parent is cancelled.

    As with the event `fetch` parameter, the references may be used
    in a chain by using the "dot" notation to fetch additional information
    about referenced institutions. For example "parent.children" will fetch
    the parent and all sibling institutions.

    .. codeauthor:: Dean Rasheed (dev-group@ucs.cam.ac.uk)
    """
    def __init__(self, conn):
        self.conn = conn

    def getInstitution(self,
                       instid,
                       fetch=None):
        """
        Get the institution with the specified ID.

        By default, only a few basic details about the institution are
        returned, but the optional `fetch` parameter may be used to
        fetch additional details.

        ``[ HTTP: GET /api/v1/institution/{instid} ]``

        **Parameters**
          `instid` : str
            [required] The ID of the institution to fetch.

          `fetch` : str
            [optional] A comma-separated list of any additional
            details to fetch.

        **Returns**
          :any:`UTBSInstitution`
            The requested institution or null if it was not found.
        """
        path = "api/v1/institution/%(instid)s"
        path_params = {"instid": instid}
        query_params = {"fetch": fetch}
        form_params = {}
        result = self.conn.invoke_method("GET", path, path_params,
                                         query_params, form_params)
        if result.error:
            raise UTBSException(result.error)
        return result.institution


class PersonMethods:
    """
    Methods for querying and manipulating people.

    .. codeauthor:: Dean Rasheed (dev-group@ucs.cam.ac.uk)
    """
    def __init__(self, conn):
        self.conn = conn

    def getPerson(self,
                  crsid,
                  fetch=None):
        """
        Get the person with the specified CRSid.

        Note that viewing people requires authentication, and a person's
        details are only visible to the following:

        * The person themselves.

        * People with the "view-person-details" privilege.

        ``[ HTTP: GET /api/v1/person/{crsid} ]``

        **Parameters**
          `crsid` : str
            [required] The CRSid of the person to fetch.

          `fetch` : str
            [optional] A comma-separated list of any additional
            details to fetch. Currently there are no additional details about
            people that can be retrieved.

        **Returns**
          :any:`UTBSPerson`
            The requested person or null if they were not found.
        """
        path = "api/v1/person/%(crsid)s"
        path_params = {"crsid": crsid}
        query_params = {"fetch": fetch}
        form_params = {}
        result = self.conn.invoke_method("GET", path, path_params,
                                         query_params, form_params)
        if result.error:
            raise UTBSException(result.error)
        return result.person

    def getCurrentBookings(self,
                           crsid,
                           fetch=None):
        """
        Get the specified person's current bookings.

        This returns all bookings whose status is not "interested",
        "cancelled" or "completed" on events that are not finished, and for
        which the participant's attendance has not yet been recorded.

        By default, only a few basic details about each booking are returned,
        but the optional `fetch` parameter may be used to fetch
        additional details about each booking.

        Note that viewing a person's bookings requires authentication as a
        user with permission to view the person's details. Additionally, the
        bookings returned are filtered according to whether the user has
        permission to view them, based on the user's privileges with respect
        to each booking's event and training provider.

        ``[ HTTP: GET /api/v1/person/{crsid}/current-bookings ]``

        **Parameters**
          `crsid` : str
            [required] The CRSid of the person.

          `fetch` : str
            [optional] A comma-separated list of any additional
            details to fetch for each booking.

        **Returns**
          list of :any:`UTBSEventBooking`
            The person's current bookings, in the order they were created.
        """
        path = "api/v1/person/%(crsid)s/current-bookings"
        path_params = {"crsid": crsid}
        query_params = {"fetch": fetch}
        form_params = {}
        result = self.conn.invoke_method("GET", path, path_params,
                                         query_params, form_params)
        if result.error:
            raise UTBSException(result.error)
        return result.bookings

    def getInterestedBookings(self,
                              crsid,
                              fetch=None):
        """
        Get the specified person's interested bookings.

        This returns all bookings whose status is "interested". These are the
        bookings created to register the person's interest in events.

        By default, only a few basic details about each booking are returned,
        but the optional `fetch` parameter may be used to fetch
        additional details about each booking.

        Note that viewing a person's bookings requires authentication as a
        user with permission to view the person's details. Additionally, the
        bookings returned are filtered according to whether the user has
        permission to view them, based on the user's privileges with respect
        to each booking's event and training provider.

        ``[ HTTP: GET /api/v1/person/{crsid}/interested-bookings ]``

        **Parameters**
          `crsid` : str
            [required] The CRSid of the person.

          `fetch` : str
            [optional] A comma-separated list of any additional
            details to fetch for each booking.

        **Returns**
          list of :any:`UTBSEventBooking`
            The person's interested bookings, in the order they were
            created.
        """
        path = "api/v1/person/%(crsid)s/interested-bookings"
        path_params = {"crsid": crsid}
        query_params = {"fetch": fetch}
        form_params = {}
        result = self.conn.invoke_method("GET", path, path_params,
                                         query_params, form_params)
        if result.error:
            raise UTBSException(result.error)
        return result.bookings

    def getTrainingHistory(self,
                           crsid,
                           fetch=None):
        """
        Get the specified person's training history.

        This returns all the person's bookings except their current bookings,
        as returned by
        :any:`getCurrentBookings() <PersonMethods.getCurrentBookings(String,String)>` and
        their bookings with a status of "interested", as returned by
        :any:`getInterestedBookings() <PersonMethods.getInterestedBookings(String,String)>`.
        Note that this may include cancelled bookings and bookings on events
        that the participant did not attend.

        By default, only a few basic details about each booking are returned,
        but the optional `fetch` parameter may be used to fetch
        additional details about each booking.

        Note that viewing a person's bookings requires authentication as a
        user with permission to view the person's details. Additionally, the
        bookings returned are filtered according to whether the user has
        permission to view them, based on the user's privileges with respect
        to each booking's event and training provider.

        ``[ HTTP: GET /api/v1/person/{crsid}/training-history ]``

        **Parameters**
          `crsid` : str
            [required] The CRSid of the person.

          `fetch` : str
            [optional] A comma-separated list of any additional
            details to fetch for each booking.

        **Returns**
          list of :any:`UTBSEventBooking`
            The person's training history, in the order in which the
            bookings were created.
        """
        path = "api/v1/person/%(crsid)s/training-history"
        path_params = {"crsid": crsid}
        query_params = {"fetch": fetch}
        form_params = {}
        result = self.conn.invoke_method("GET", path, path_params,
                                         query_params, form_params)
        if result.error:
            raise UTBSException(result.error)
        return result.bookings


class ProgrammeMethods:
    """
    Methods for querying and manipulating programmes.

    **The fetch parameter for programmes**

    All methods that return programmes also accept an optional
    `fetch` parameter that may be used to request additional
    information about the programmes returned. For more details about the
    general rules that apply to the `fetch` parameter, refer to the
    :any:`EventMethods` documentation.

    For programmes the `fetch` parameter may be used to fetch
    referenced providers and events. The following references are supported:

    * ``"provider"`` - fetches the programme's provider.

    * ``"events"`` - fetches all the programme's events, in (date, ID)
      order.

    As with the event `fetch` parameter, the references may be used
    in a chain by using the "dot" notation to fetch additional information
    about referenced providers and events. For example "events.themes" will
    fetch all the themes for each of the programme's events. For more
    information about what can be fetched from referenced providers and
    events, refer to the documentation for :any:`ProviderMethods` and
    :any:`EventMethods`.

    .. codeauthor:: Dean Rasheed (dev-group@ucs.cam.ac.uk)
    """
    def __init__(self, conn):
        self.conn = conn

    def getProgrammesByDate(self,
                            startDate=None,
                            endDate=None,
                            fetch=None):
        """
        Get all programmes in the specified date range.

        This will return any programmes that overlap the specified date range.
        More specifically, it will return programmes whose start is less than
        or equal to the end date specified, and whose end is greater than or
        equal to the start date specified (i.e., all the start and end dates
        are treated inclusively).

        By default, only a few basic details about each programme are
        returned, but the optional `fetch` parameter may be used to
        fetch additional attributes or references.

        .. note::
          When using this API directly via the URL endpoints, date
          parameters should be supplied as either milliseconds since epoch, or
          as ISO 8601 formatted date or date-time strings.

        ``[ HTTP: GET /api/v1/programme/programmes-by-date ]``

        **Parameters**
          `startDate` : date
            [optional] The start date. If omitted, this will
            default to today.

          `endDate` : date
            [optional] The end date. If omitted, this will default
            to today.

          `fetch` : str
            [optional] A comma-separated list of any additional
            details to fetch for each programme.

        **Returns**
          list of :any:`UTBSProgramme`
            A list of programmes found, in (date, ID) order.
        """
        path = "api/v1/programme/programmes-by-date"
        path_params = {}
        query_params = {"startDate": startDate,
                        "endDate": endDate,
                        "fetch": fetch}
        form_params = {}
        result = self.conn.invoke_method("GET", path, path_params,
                                         query_params, form_params)
        if result.error:
            raise UTBSException(result.error)
        return result.programmes

    def getProgramme(self,
                     id,
                     fetch=None):
        """
        Get the programme with the specified ID.

        By default, only a few basic details about the programme are returned,
        but the optional `fetch` parameter may be used to fetch
        additional details.

        ``[ HTTP: GET /api/v1/programme/{id} ]``

        **Parameters**
          `id` : long
            [required] The ID of the programme to fetch.

          `fetch` : str
            [optional] A comma-separated list of any additional
            details to fetch.

        **Returns**
          :any:`UTBSProgramme`
            The requested programme or null if it was not found.
        """
        path = "api/v1/programme/%(id)s"
        path_params = {"id": id}
        query_params = {"fetch": fetch}
        form_params = {}
        result = self.conn.invoke_method("GET", path, path_params,
                                         query_params, form_params)
        if result.error:
            raise UTBSException(result.error)
        return result.programme

    def getEventsInTimePeriod(self,
                              id,
                              start=None,
                              end=None,
                              checkSessions=None,
                              fetch=None):
        """
        Get a programme's events in the specified time period.

        This will return any events that overlap the specified time period.
        More specifically, it will return events whose start is less than or
        equal to the end of the time period, and whose end is greater than or
        equal to the start of the time period (i.e., all the start and end
        timestamps are treated inclusively).

        Optionally, this will also check the event's sessions and exclude any
        events that have no sessions overlapping the specified time period.
        This can happen for events with multiple sessions. For example,
        suppose an event has 2 sessions, one on Monday and the other on
        Friday, and that the specified time period to search was on Wednesday.
        Then by default, the event would be returned, because it starts on
        Monday and ends on Friday, which overlaps the time period being
        searched, but if session checking is enabled, the event would be
        excluded.

        By default, only a few basic details about each event are returned,
        but the optional `fetch` parameter may be used to fetch
        additional attributes or references.

        .. note::
          When using this API directly via the URL endpoints, date-time
          parameters should be supplied as either milliseconds since epoch, or
          as ISO 8601 formatted date or date-time strings.

        ``[ HTTP: GET /api/v1/programme/{id}/events-in-time-period ]``

        **Parameters**
          `id` : long
            [required] The ID of the programme.

          `start` : datetime
            [optional] The start of the time period to search. If
            omitted, this will default to 0:00am today.

          `end` : datetime
            [optional] The end of the time period to search. If
            omitted, this will default to the first midnight after the start date.

          `checkSessions` : bool
            [optional] If :any:`True`, check the event
            sessions, and exclude any events that have no sessions overlapping the
            the time period being searched.

          `fetch` : str
            [optional] A comma-separated list of any additional
            details to fetch for each event.

        **Returns**
          list of :any:`UTBSEvent`
            A list of events found, in (start date-time, ID) order.
        """
        path = "api/v1/programme/%(id)s/events-in-time-period"
        path_params = {"id": id}
        query_params = {"start": start,
                        "end": end,
                        "checkSessions": checkSessions,
                        "fetch": fetch}
        form_params = {}
        result = self.conn.invoke_method("GET", path, path_params,
                                         query_params, form_params)
        if result.error:
            raise UTBSException(result.error)
        return result.events

    def getSelfTaughtEvents(self,
                            id,
                            fetch=None):
        """
        Get a programme's self-taught events.

        By default, only a few basic details about each event are returned,
        but the optional `fetch` parameter may be used to fetch
        additional attributes or references.

        ``[ HTTP: GET /api/v1/programme/{id}/self-taught-events ]``

        **Parameters**
          `id` : long
            [required] The ID of the programme.

          `fetch` : str
            [optional] A comma-separated list of any additional
            details to fetch for each event.

        **Returns**
          list of :any:`UTBSEvent`
            A list of self-taught events, in (title, ID) order.
        """
        path = "api/v1/programme/%(id)s/self-taught-events"
        path_params = {"id": id}
        query_params = {"fetch": fetch}
        form_params = {}
        result = self.conn.invoke_method("GET", path, path_params,
                                         query_params, form_params)
        if result.error:
            raise UTBSException(result.error)
        return result.events


class ProviderMethods:
    """
    Methods for querying and manipulating training providers.

    **The fetch parameter for providers**

    All methods that return providers also accept an optional
    `fetch` parameter that may be used to request additional
    information about the providers returned. For more details about the
    general rules that apply to the `fetch` parameter, refer to the
    :any:`EventMethods` documentation.

    For providers the `fetch` parameter may be used to fetch
    referenced programmes, themes and venues. The following references are
    supported:

    * ``"institution"`` - fetches full details about the provider's
      institution.

    * ``"current_programme"`` - fetches the provider's current
      programme, if there is one.

    * ``"programmes"`` - fetches all the provider's programmes, in date
      order.

    * ``"themes"`` - fetches all the provider's themes.

    * ``"venues"`` - fetches all the venues belonging to the
      provider.

    As with the event `fetch` parameter, the references may be used
    in a chain by using the "dot" notation to fetch additional information
    about referenced programmes, themes and venues. For example
    "themes.courses" will fetch all the courses for each of the provider's
    themes. For more information about what can be fetched from referenced
    programmes, themes and venues, refer to the documentation for
    :any:`ProgrammeMethods`, :any:`ThemeMethods` and :any:`VenueMethods`.

    .. codeauthor:: Dean Rasheed (dev-group@ucs.cam.ac.uk)
    """
    def __init__(self, conn):
        self.conn = conn

    def getAllProviders(self,
                        fetch=None):
        """
        Return a list of all training providers.

        By default, only a few basic details about each provider are returned,
        but the optional `fetch` parameter may be used to fetch
        additional attributes or references.

        ``[ HTTP: GET /api/v1/provider/all-providers ]``

        **Parameters**
          `fetch` : str
            [optional] A comma-separated list of any additional
            details to fetch for each provider.

        **Returns**
          list of :any:`UTBSProvider`
            The requested providers (in name order).
        """
        path = "api/v1/provider/all-providers"
        path_params = {}
        query_params = {"fetch": fetch}
        form_params = {}
        result = self.conn.invoke_method("GET", path, path_params,
                                         query_params, form_params)
        if result.error:
            raise UTBSException(result.error)
        return result.providers

    def getProvider(self,
                    shortName,
                    fetch=None):
        """
        Get the training provider with the specified short name (e.g., "UCS").

        By default, only a few basic details about the provider are returned,
        but the optional `fetch` parameter may be used to fetch
        additional attributes or references.

        ``[ HTTP: GET /api/v1/provider/{shortName} ]``

        **Parameters**
          `shortName` : str
            [required] The short name of the provider to fetch.

          `fetch` : str
            [optional] A comma-separated list of any additional
            details to fetch.

        **Returns**
          :any:`UTBSProvider`
            The requested provider or null if it was not found.
        """
        path = "api/v1/provider/%(shortName)s"
        path_params = {"shortName": shortName}
        query_params = {"fetch": fetch}
        form_params = {}
        result = self.conn.invoke_method("GET", path, path_params,
                                         query_params, form_params)
        if result.error:
            raise UTBSException(result.error)
        return result.provider

    def getEventsInTimePeriod(self,
                              shortName,
                              start=None,
                              end=None,
                              checkSessions=None,
                              fetch=None):
        """
        Get the events run by the specified training provider in the specified
        time period.

        This will return any events that overlap the specified time period.
        More specifically, it will return events whose start is less than or
        equal to the end of the time period, and whose end is greater than or
        equal to the start of the time period (i.e., all the start and end
        timestamps are treated inclusively).

        Optionally, this will also check the event's sessions and exclude any
        events that have no sessions overlapping the specified time period.
        This can happen for events with multiple sessions. For example,
        suppose an event has 2 sessions, one on Monday and the other on
        Friday, and that the specified time period to search was on Wednesday.
        Then by default, the event would be returned, because it starts on
        Monday and ends on Friday, which overlaps the time period being
        searched, but if session checking is enabled, the event would be
        excluded.

        By default, only a few basic details about each event are returned,
        but the optional `fetch` parameter may be used to fetch
        additional attributes or references.

        .. note::
          When using this API directly via the URL endpoints, date-time
          parameters should be supplied as either milliseconds since epoch, or
          as ISO 8601 formatted date or date-time strings.

        ``[ HTTP: GET /api/v1/provider/{shortName}/events-in-time-period ]``

        **Parameters**
          `shortName` : str
            [required] The short name of the provider.

          `start` : datetime
            [optional] The start of the time period to search. If
            omitted, this will default to 0:00am today.

          `end` : datetime
            [optional] The end of the time period to search. If
            omitted, this will default to the first midnight after the start date.

          `checkSessions` : bool
            [optional] If :any:`True`, check the event
            sessions, and exclude any events that have no sessions overlapping the
            the time period being searched.

          `fetch` : str
            [optional] A comma-separated list of any additional
            details to fetch for each event.

        **Returns**
          list of :any:`UTBSEvent`
            A list of events found, in (start date-time, ID) order.
        """
        path = "api/v1/provider/%(shortName)s/events-in-time-period"
        path_params = {"shortName": shortName}
        query_params = {"start": start,
                        "end": end,
                        "checkSessions": checkSessions,
                        "fetch": fetch}
        form_params = {}
        result = self.conn.invoke_method("GET", path, path_params,
                                         query_params, form_params)
        if result.error:
            raise UTBSException(result.error)
        return result.events

    def getProgrammes(self,
                      shortName,
                      fetch=None):
        """
        Get all the programmes run by the specified training provider.

        By default, only a few basic details about each programme are
        returned, but the optional `fetch` parameter may be used to
        fetch additional attributes or references of each programme.

        ``[ HTTP: GET /api/v1/provider/{shortName}/programmes ]``

        **Parameters**
          `shortName` : str
            [required] The short name of the provider.

          `fetch` : str
            [optional] A comma-separated list of any additional
            details to fetch for each programme.

        **Returns**
          list of :any:`UTBSProgramme`
            The provider's programmes, in (date, ID) order.
        """
        path = "api/v1/provider/%(shortName)s/programmes"
        path_params = {"shortName": shortName}
        query_params = {"fetch": fetch}
        form_params = {}
        result = self.conn.invoke_method("GET", path, path_params,
                                         query_params, form_params)
        if result.error:
            raise UTBSException(result.error)
        return result.programmes

    def getProgrammesByDate(self,
                            shortName,
                            startDate=None,
                            endDate=None,
                            fetch=None):
        """
        Get the programmes run by the specified training provider in the
        specified date range.

        This will return any programmes that overlap the specified date range.
        More specifically, it will return programmes whose start is less than
        or equal to the end date specified, and whose end is greater than or
        equal to the start date specified (i.e., all the start and end dates
        are treated inclusively).

        By default, only a few basic details about each programme are
        returned, but the optional `fetch` parameter may be used to
        fetch additional attributes or references.

        .. note::
          When using this API directly via the URL endpoints, date
          parameters should be supplied as either milliseconds since epoch, or
          as ISO 8601 formatted date or date-time strings.

        ``[ HTTP: GET /api/v1/provider/{shortName}/programmes-by-date ]``

        **Parameters**
          `shortName` : str
            [required] The short name of the provider.

          `startDate` : date
            [optional] The start date. If omitted, this will
            default to today.

          `endDate` : date
            [optional] The end date. If omitted, this will default
            to today.

          `fetch` : str
            [optional] A comma-separated list of any additional
            details to fetch for each programme.

        **Returns**
          list of :any:`UTBSProgramme`
            A list of programmes found, in (date, ID) order.
        """
        path = "api/v1/provider/%(shortName)s/programmes-by-date"
        path_params = {"shortName": shortName}
        query_params = {"startDate": startDate,
                        "endDate": endDate,
                        "fetch": fetch}
        form_params = {}
        result = self.conn.invoke_method("GET", path, path_params,
                                         query_params, form_params)
        if result.error:
            raise UTBSException(result.error)
        return result.programmes

    def getThemes(self,
                  shortName,
                  fetch=None):
        """
        Get all the themes belonging to the specified training provider.

        By default, only a few basic details about each theme are returned,
        but the optional `fetch` parameter may be used to fetch
        additional attributes or references of each programme.

        ``[ HTTP: GET /api/v1/provider/{shortName}/themes ]``

        **Parameters**
          `shortName` : str
            [required] The short name of the provider.

          `fetch` : str
            [optional] A comma-separated list of any additional
            details to fetch for each theme.

        **Returns**
          list of :any:`UTBSTheme`
            The provider's themes, in title order.
        """
        path = "api/v1/provider/%(shortName)s/themes"
        path_params = {"shortName": shortName}
        query_params = {"fetch": fetch}
        form_params = {}
        result = self.conn.invoke_method("GET", path, path_params,
                                         query_params, form_params)
        if result.error:
            raise UTBSException(result.error)
        return result.themes

    def getVenues(self,
                  shortName,
                  fetch=None):
        """
        Get all the venues belonging to the specified training provider.

        By default, only a few basic details about each venue are returned,
        but the optional `fetch` parameter may be used to fetch
        additional attributes or references of each programme.

        ``[ HTTP: GET /api/v1/provider/{shortName}/venues ]``

        **Parameters**
          `shortName` : str
            [required] The short name of the provider.

          `fetch` : str
            [optional] A comma-separated list of any additional
            details to fetch for each venue.

        **Returns**
          list of :any:`UTBSVenue`
            The provider's venues, in name order.
        """
        path = "api/v1/provider/%(shortName)s/venues"
        path_params = {"shortName": shortName}
        query_params = {"fetch": fetch}
        form_params = {}
        result = self.conn.invoke_method("GET", path, path_params,
                                         query_params, form_params)
        if result.error:
            raise UTBSException(result.error)
        return result.venues


class ThemeMethods:
    """
    Methods for querying and manipulating themes.

    **The fetch parameter for themes**

    All methods that return themes also accept an optional `fetch`
    parameter that may be used to request additional information about the
    themes returned. For more details about the general rules that apply to
    the `fetch` parameter, refer to the :any:`EventMethods`
    documentation.

    For themes the `fetch` parameter may be used to fetch
    referenced providers, courses and events. The following references are
    supported:

    * ``"provider"`` - fetches the theme's provider.

    * ``"courses"`` - fetches the theme's courses.

    * ``"events"`` - fetches all the theme's events, in (date, ID)
      order.

    As with the event `fetch` parameter, the references may be used
    in a chain by using the "dot" notation to fetch additional information
    about referenced providers, courses and events. For example
    "events.sessions" will fetch all the theme's events and all their
    sessions. For more information about what can be fetched from referenced
    providers, courses and events, refer to the documentation for
    :any:`ProviderMethods`, :any:`CourseMethods` and :any:`EventMethods`.

    .. codeauthor:: Dean Rasheed (dev-group@ucs.cam.ac.uk)
    """
    def __init__(self, conn):
        self.conn = conn

    def getAllThemes(self,
                     fetch=None):
        """
        Return a list of all themes.

        By default, only a few basic details about each theme are returned,
        but the optional `fetch` parameter may be used to fetch
        additional attributes or references.

        ``[ HTTP: GET /api/v1/theme/all-themes ]``

        **Parameters**
          `fetch` : str
            [optional] A comma-separated list of any additional
            details to fetch for each theme.

        **Returns**
          list of :any:`UTBSTheme`
            The requested themes, in title order.
        """
        path = "api/v1/theme/all-themes"
        path_params = {}
        query_params = {"fetch": fetch}
        form_params = {}
        result = self.conn.invoke_method("GET", path, path_params,
                                         query_params, form_params)
        if result.error:
            raise UTBSException(result.error)
        return result.themes

    def getThemesByName(self,
                        name,
                        provName=None,
                        fetch=None):
        """
        Get the themes with the specified name.

        There may be multiple themes with the same name belonging to different
        training providers. If a provider name is also supplied, then at most
        one theme will be returned.

        ``[ HTTP: GET /api/v1/theme/get-by-name/{name} ]``

        **Parameters**
          `name` : str
            [required] The short name of the themes to fetch.

          `provName` : str
            [optional] The short name of a training provider.

          `fetch` : str
            [optional] A comma-separated list of any additional
            details to fetch for each theme.

        **Returns**
          list of :any:`UTBSTheme`
            The matching themes, in title order.
        """
        path = "api/v1/theme/get-by-name/%(name)s"
        path_params = {"name": name}
        query_params = {"provName": provName,
                        "fetch": fetch}
        form_params = {}
        result = self.conn.invoke_method("GET", path, path_params,
                                         query_params, form_params)
        if result.error:
            raise UTBSException(result.error)
        return result.themes

    def getTheme(self,
                 id,
                 fetch=None):
        """
        Get the theme with the specified ID.

        By default, only a few basic details about the theme are returned, but
        the optional `fetch` parameter may be used to fetch
        additional details, such as the events in the theme.

        ``[ HTTP: GET /api/v1/theme/{id} ]``

        **Parameters**
          `id` : long
            [required] The ID of the theme to fetch.

          `fetch` : str
            [optional] A comma-separated list of any additional
            details to fetch.

        **Returns**
          :any:`UTBSTheme`
            The requested theme or null if it was not found.
        """
        path = "api/v1/theme/%(id)s"
        path_params = {"id": id}
        query_params = {"fetch": fetch}
        form_params = {}
        result = self.conn.invoke_method("GET", path, path_params,
                                         query_params, form_params)
        if result.error:
            raise UTBSException(result.error)
        return result.theme

    def getEventsInTimePeriod(self,
                              id,
                              start=None,
                              end=None,
                              checkSessions=None,
                              fetch=None):
        """
        Get the events in the specified theme in the specified time period.

        This will return any events that overlap the specified time period.
        More specifically, it will return events whose start is less than or
        equal to the end of the time period, and whose end is greater than or
        equal to the start of the time period (i.e., all the start and end
        timestamps are treated inclusively).

        Optionally, this will also check the event's sessions and exclude any
        events that have no sessions overlapping the specified time period.
        This can happen for events with multiple sessions. For example,
        suppose an event has 2 sessions, one on Monday and the other on
        Friday, and that the specified time period to search was on Wednesday.
        Then by default, the event would be returned, because it starts on
        Monday and ends on Friday, which overlaps the time period being
        searched, but if session checking is enabled, the event would be
        excluded.

        By default, only a few basic details about each event are returned,
        but the optional `fetch` parameter may be used to fetch
        additional attributes or references.

        .. note::
          When using this API directly via the URL endpoints, date-time
          parameters should be supplied as either milliseconds since epoch, or
          as ISO 8601 formatted date or date-time strings.

        ``[ HTTP: GET /api/v1/theme/{id}/events-in-time-period ]``

        **Parameters**
          `id` : long
            [required] The ID of the theme.

          `start` : datetime
            [optional] The start of the time period to search. If
            omitted, this will default to 0:00am today.

          `end` : datetime
            [optional] The end of the time period to search. If
            omitted, this will default to the first midnight after the start date.

          `checkSessions` : bool
            [optional] If :any:`True`, check the event
            sessions, and exclude any events that have no sessions overlapping the
            the time period being searched.

          `fetch` : str
            [optional] A comma-separated list of any additional
            details to fetch for each event.

        **Returns**
          list of :any:`UTBSEvent`
            A list of events found, in (start date-time, ID) order.
        """
        path = "api/v1/theme/%(id)s/events-in-time-period"
        path_params = {"id": id}
        query_params = {"start": start,
                        "end": end,
                        "checkSessions": checkSessions,
                        "fetch": fetch}
        form_params = {}
        result = self.conn.invoke_method("GET", path, path_params,
                                         query_params, form_params)
        if result.error:
            raise UTBSException(result.error)
        return result.events

    def getSelfTaughtEvents(self,
                            id,
                            fetch=None):
        """
        Get a theme's self-taught events.

        By default, only a few basic details about each event are returned,
        but the optional `fetch` parameter may be used to fetch
        additional attributes or references.

        ``[ HTTP: GET /api/v1/theme/{id}/self-taught-events ]``

        **Parameters**
          `id` : long
            [required] The ID of the theme.

          `fetch` : str
            [optional] A comma-separated list of any additional
            details to fetch for each event.

        **Returns**
          list of :any:`UTBSEvent`
            A list of self-taught events, in (title, ID) order.
        """
        path = "api/v1/theme/%(id)s/self-taught-events"
        path_params = {"id": id}
        query_params = {"fetch": fetch}
        form_params = {}
        result = self.conn.invoke_method("GET", path, path_params,
                                         query_params, form_params)
        if result.error:
            raise UTBSException(result.error)
        return result.events


class VenueMethods:
    """
    Methods for querying and manipulating venues.

    **The fetch parameter for venues**

    All methods that return venues also accept an optional `fetch`
    parameter that may be used to request additional information about the
    venues returned. For more details about the general rules that apply to
    the `fetch` parameter, refer to the :any:`EventMethods`
    documentation.

    For venues the `fetch` parameter may be used to fetch the
    venue's provider:

    * ``"provider"`` - fetches the venue's provider.

    As with the event `fetch` parameter, this reference may be used
    in a chain by using the "dot" notation to fetch additional information
    about referenced provider. For example "provider.venues" will fetch the
    venue's provider and all the other venues belonging to that provider. For
    more information about what can be fetched from the referenced provider,
    refer to the documentation for :any:`ProviderMethods`.

    .. codeauthor:: Dean Rasheed (dev-group@ucs.cam.ac.uk)
    """
    def __init__(self, conn):
        self.conn = conn

    def getAllVenues(self,
                     fetch=None):
        """
        Return a list of all venues.

        By default, only a few basic details about each venue are returned,
        but the optional `fetch` parameter may be used to fetch
        additional attributes or references.

        ``[ HTTP: GET /api/v1/venue/all-venues ]``

        **Parameters**
          `fetch` : str
            [optional] A comma-separated list of any additional
            details to fetch for each venue.

        **Returns**
          list of :any:`UTBSVenue`
            The requested venues, in name order.
        """
        path = "api/v1/venue/all-venues"
        path_params = {}
        query_params = {"fetch": fetch}
        form_params = {}
        result = self.conn.invoke_method("GET", path, path_params,
                                         query_params, form_params)
        if result.error:
            raise UTBSException(result.error)
        return result.venues

    def getVenue(self,
                 id,
                 fetch=None):
        """
        Get the venue with the specified ID.

        By default, only a few basic details about the venue are returned, but
        the optional `fetch` parameter may be used to fetch
        additional attributes or references.

        ``[ HTTP: GET /api/v1/venue/{id} ]``

        **Parameters**
          `id` : long
            [required] The ID of the venue to fetch.

          `fetch` : str
            [optional] A comma-separated list of any additional
            details to fetch.

        **Returns**
          :any:`UTBSVenue`
            The requested venue or null if it was not found.
        """
        path = "api/v1/venue/%(id)s"
        path_params = {"id": id}
        query_params = {"fetch": fetch}
        form_params = {}
        result = self.conn.invoke_method("GET", path, path_params,
                                         query_params, form_params)
        if result.error:
            raise UTBSException(result.error)
        return result.venue

    def getEventsInTimePeriod(self,
                              id,
                              start=None,
                              end=None,
                              checkSessions=None,
                              fetch=None):
        """
        Get the events held in a venue in the specified time period.

        This will return any events that overlap the specified time period.
        More specifically, it will return events whose start is less than or
        equal to the end of the time period, and whose end is greater than or
        equal to the start of the time period (i.e., all the start and end
        timestamps are treated inclusively).

        Optionally, this will also check the event's sessions and exclude any
        events that have no sessions overlapping the specified time period.
        This can happen for events with multiple sessions. For example,
        suppose an event has 2 sessions, one on Monday and the other on
        Friday, and that the specified time period to search was on Wednesday.
        Then by default, the event would be returned, because it starts on
        Monday and ends on Friday, which overlaps the time period being
        searched, but if session checking is enabled, the event would be
        excluded.

        By default, only a few basic details about each event are returned,
        but the optional `fetch` parameter may be used to fetch
        additional attributes or references.

        .. note::
          When using this API directly via the URL endpoints, date-time
          parameters should be supplied as either milliseconds since epoch, or
          as ISO 8601 formatted date or date-time strings.

        ``[ HTTP: GET /api/v1/venue/{id}/events-in-time-period ]``

        **Parameters**
          `id` : long
            [required] The ID of the venue.

          `start` : datetime
            [optional] The start of the time period to search. If
            omitted, this will default to 0:00am today.

          `end` : datetime
            [optional] The end of the time period to search. If
            omitted, this will default to the first midnight after the start date.

          `checkSessions` : bool
            [optional] If :any:`True`, check the event
            sessions, and exclude any events that have no sessions overlapping the
            the time period being searched.

          `fetch` : str
            [optional] A comma-separated list of any additional
            details to fetch for each event.

        **Returns**
          list of :any:`UTBSEvent`
            A list of events found, in (start date-time, ID) order.
        """
        path = "api/v1/venue/%(id)s/events-in-time-period"
        path_params = {"id": id}
        query_params = {"start": start,
                        "end": end,
                        "checkSessions": checkSessions,
                        "fetch": fetch}
        form_params = {}
        result = self.conn.invoke_method("GET", path, path_params,
                                         query_params, form_params)
        if result.error:
            raise UTBSException(result.error)
        return result.events
